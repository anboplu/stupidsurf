#include "RtAudio.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>

#include <limits.h>
#include <time.h>
#include <math.h>

#include "GuiSdl.h"
#include "SurfDrawing.h"
using namespace Gui;

RtAudioFormat RT_READ_UNIT(){return RTAUDIO_FLOAT32;}

const unsigned sampleRate = 44100;
const unsigned bufferFrames = 1024;

READ_UNIT * gInput = NULL;


template<class T>
T MyAbs(T t){return t >= 0 ? t : -t;}

template<class T>
struct MaxTracker
{
    bool bInit;
    T val;

    MaxTracker():bInit(false){}
    MaxTracker(T t):bInit(true), val(t){}

    void Boom(T t)
    {
        if(!bInit || t > val)
        {
            bInit = true;
            val = t;
        }
    }
};

struct BarDrawer
{
    Point pOffset;
    Size sz;
    unsigned nSpacing;
    Color c;

    void Draw(SP<SdlGraphicalInterface> pGr, std::vector<float>& v)
    {
        for(unsigned i = 0; i < v.size(); ++i)
            pGr->DrawRectangle(Rectangle(pOffset.x + (i - v.size()/2) * (sz.x + nSpacing),
                                     Crd(pOffset.y - v[i]*sz.y),
                                         pOffset.x + (i - v.size()/2) * (sz.x + nSpacing) + sz.x,
                                         pOffset.y),
                               c, false);
    }
};

float goertzel(READ_UNIT *x, int N, float frequency, int samplerate) {
    float Skn, Skn1, Skn2;
    Skn = Skn1 = Skn2 = 0;
    
    for (int i=0; i<N; i++) {
    Skn2 = Skn1;
    Skn1 = Skn;
    Skn = 2*cos(2*3.14159265F*frequency/samplerate)*Skn1 - Skn2 + x[i];
    }
    
    float WNk = exp(-2*3.14159265F*frequency/samplerate); // this one ignores complex stuff
    //float WNk = exp(-2*j*PI*k/N);
    return (Skn - WNk*Skn1);
}

READ_UNIT fourier_cos(READ_UNIT *x, int N, float frequency, int samplerate) {
    READ_UNIT fRet = 0;
    for(int i = 0; i < N; ++i)
        fRet += x[i] * cos(i * 2*3.14159265F * frequency/samplerate);
    return fRet / N * 2;
}

READ_UNIT fourier_sin(READ_UNIT *x, int N, float frequency, int samplerate) {
    READ_UNIT fRet = 0;
    for(int i = 0; i < N; ++i)
        fRet += x[i] * sin(i * 2*3.14159265F * frequency/samplerate);
    return fRet / N * 2;
}

std::vector<float> vData1;
std::vector<float> vData2;
std::vector<float> vFreq;

const float frMiddleC = 261.626F;

float GetFrequency(int n)
{
    return frMiddleC * std::pow(2, float(n)/12); 
}

int record( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
         double streamTime, RtAudioStreamStatus status, void *userData )
{
    if ( status )
        std::cout << "Stream overflow detected!" << std::endl;

    READ_UNIT* pInputBuffer = reinterpret_cast<READ_UNIT*>(inputBuffer);

	//should lock this
	if(gInput == NULL)
		gInput = (READ_UNIT*)malloc(nBufferFrames*sizeof(READ_UNIT));
	memcpy(gInput,inputBuffer,nBufferFrames*sizeof(READ_UNIT));


    return 0;
}

int main(int argc, char *argv[])
{
    srand( (unsigned)time( NULL ));

	SDL_Event event;
    bool bExit = false;

    for(int i = -48; i <= 72; ++i)
    {
        vData1.push_back(0);
        vData2.push_back(0);
        vFreq.push_back(GetFrequency(i));
    }


    RtAudio adc;

    try
    {
        Rectangle sBound = Rectangle(0, 0, 1500, 480);
        SP<SdlGraphicalInterface> pGr = new SdlGraphicalInterface(sBound.sz);


        if (adc.getDeviceCount() < 1)
        {
            std::cout << "No working microphone found\n";
            return -1;
        }

        RtAudio::StreamParameters parameters;
        parameters.deviceId = adc.getDefaultInputDevice();
        parameters.nChannels = 2;
        parameters.firstChannel = 0;

        unsigned myBufferFrames = bufferFrames;

        RtAudio::StreamOptions options;
        options.flags = RTAUDIO_NONINTERLEAVED;

        adc.openStream( NULL, &parameters, RT_READ_UNIT(),
                        sampleRate, &myBufferFrames, &record, 0, &options );
        adc.startStream();

        BarDrawer bd1;
        bd1.pOffset = Point(sBound.sz.x/2, sBound.sz.y*4/5);
        bd1.sz = Size(sBound.sz.x/300, sBound.sz.y/2);
        bd1.nSpacing = bd1.sz.x/2;
        bd1.c = Color(255, 0, 0);

        BarDrawer bd2;
        bd2.pOffset = Point(sBound.sz.x/2, sBound.sz.y*2/5);
        bd2.sz = Size(sBound.sz.x/300, sBound.sz.y/2);
        bd2.nSpacing = bd2.sz.x/2;
        bd2.c = Color(0, 0, 255);

        Uint32 nTimer = SDL_GetTicks();

        while(!bExit)
        {
		    if(SDL_GetTicks() - nTimer > 100)
            {
                nTimer = SDL_GetTicks();
                pGr->DrawRectangle(sBound, Color(255,255,255), false);
                bd1.Draw(pGr, vData1);
                bd2.Draw(pGr, vData2);
				//drawLine(pGr, 10,0,11,100);
				//drawFunction(pGr, &cos, Point(50,300),100,0,50,10,100);
				drawLane(pGr,nTimer,200,3000,50,1000);
				//should use locks here but whatever
				if(gInput != NULL)
					drawArray<READ_UNIT *>(pGr,gInput,bufferFrames,0,Point(0,300),1,400);
                pGr->RefreshAll();
            }
            
            if(!SDL_PollEvent(&event))
                continue;

            int i = event.type;
            
            if(i == SDL_QUIT)
                break;
            else if(i == SDL_KEYDOWN)
            {
                if(event.key.keysym.sym == SDLK_ESCAPE)
                    break;
            }
            else if(i == SDL_KEYUP){}
	    }

    }
    catch(RtError& e)
    {
        e.printMessage();
        return -1;
    }
    catch(MyException& me)
    {
        std::cout << me.GetDescription(true) << "\n";
    }
    catch(...)
    {
        std::cout << "Unknown error!\n";
    }

    try
    {
        adc.stopStream();
    }
    catch (RtError& e)
    {
        e.printMessage();
    }

    if (adc.isStreamOpen())
        adc.closeStream();

    if(nGlobalSuperMegaCounter != 0)
        std::cout << "Memory Leak: " << nGlobalSuperMegaCounter << "\n"; 

    return 0;
}