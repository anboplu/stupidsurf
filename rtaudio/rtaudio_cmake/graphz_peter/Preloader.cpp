#include "Preloader.h"

std::ostream& operator << (std::ostream& ofs, const FilePath& fp)
{
    ofs << "SYSTEM " << fp.bInLinux << "\nPATH " << fp.sPath << "\n";

    return ofs;
}

std::istream& operator >> (std::istream& ifs, FilePath& fp)
{
    ParsePosition("SYSTEM", ifs);
    
    ifs >> fp.bInLinux;

    ParseGrabLine("PATH", ifs, fp.sPath);

    fp.Slash(fp.sPath);

    return ifs;
}

bool ParsePosition(std::string sToken, std::istream& ifs)
{
    std::string s;

    while(true)
    {
        if(! (ifs >> s) )
            return false;
        
        if (s == sToken)
            return true;
    }
}

bool ParseGrabNext(std::string sToken, std::istream& ifs, std::string& sResult)
{
    std::string s;

    while(true)
    {
        if(! (ifs >> s) )
            return false;
        
        if (s == sToken)
            return (ifs >> sResult).fail();
    }
}

bool ParseGrabLine(std::string sToken, std::istream& ifs, std::string& sResult)
{
    std::string s;

    while(true)
    {
        if(! (ifs >> s) )
            return false;
        
        if (s == sToken)
        {
            char c;
            ifs >> c;   // skip spaces, tabs etcS
            ifs.putback(c);
            
            return std::getline(ifs, sResult).fail();
        }
    }
}
