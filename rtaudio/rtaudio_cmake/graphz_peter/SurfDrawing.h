#pragma once
#include <vector>
#include "GuiSdl.h"
#include "GuiGen.h"
typedef float READ_UNIT;
using namespace Gui;
class BackgroundSoundWaveImage
{
	std::vector<READ_UNIT> mData;
	unsigned ptIndex;
	unsigned chunkIndex;
	unsigned chunks;
	unsigned subChunks;
	unsigned totalChunks;
public:
	BackgroundSoundWaveImage(int nBufferFrames):chunkIndex(0),ptIndex(0),subChunks(nBufferFrames)
	{
		//chunks needs to be afunction of nBufferFRames so that we have enough chunks to span the whole screen without running out of data
		chunks = 10;
		totalChunks = subChunks*chunks;
		//initialize the vector
		mData = std::vector<READ_UNIT>(chunks*subChunks);
	}
	~BackgroundSoundWaveImage();
	void setData( void *inputBuffer, unsigned int nBufferFrames)
	{
		advancechunkIndex();
		if(nBufferFrames!=subChunks)
			throw SimpleException("nBufferFrames does not matched backgroundsoundwaveimage nbufferframes");
		//shady stuff that I'm doing just to annoy anton
		//also slightly more efficient
		memcpy(&(getHead()[0]),inputBuffer,nBufferFrames*sizeof(READ_UNIT));
	}
	void drawData()
	{
		//
	}
private:
	inline unsigned crementPtIndex(int amnt)
	{
		ptIndex += amnt;
		if(ptIndex > totalChunks-1)
			ptIndex %= totalChunks;
		else if(ptIndex < 0)
			ptIndex = (ptIndex + amnt/totalChunks*totalChunks)%totalChunks;
	}
	READ_UNIT* getHead()
	{
		return &mData[chunkIndex*subChunks];
	}
	READ_UNIT* getNextHead()
	{
		return &mData[((chunkIndex+1)%chunks)*subChunks];
	}
	READ_UNIT* getPrevHead()
	{
		return &mData[((chunkIndex-1+chunks)%chunks)*subChunks];
	}
	void advancechunkIndex()
	{
		(chunkIndex += 1) %= chunks;
	}
};
void swap(int & x, int & y)
{
	int z = y;
	y = x;
	x = z;
}

void plot(SP<SdlGraphicalInterface> sgi, int x, int y)
{
	sgi->DrawRectangle(Rectangle(Point(x,y),Size(1,1)),Color(0,0,0),false);
}
void drawLine(SP<SdlGraphicalInterface> sgi, int x0, int y0, int x1, int y1 /*, surface */)
{
	bool steep = abs(y1-y0)>abs(x1-x0);
	if(steep)
	{
		swap(x0, y0);
        swap(x1, y1);
	}
	if(x0 > x1)
	{
		swap(x0,x1);
		swap(y0,y1);
	}
	int deltax = x1 -x0;
	int deltay = abs(y1 - y0);
	int error = deltax/2;
	int ystep;
	int y = y0;
	if(y0<y1) ystep = 1;
	else ystep = -1;
	int x = x0;
	while(x <= x1)
	{
		if(steep) plot(sgi,y,x);
		else plot(sgi,x,y);
		error -= deltay;
		if(error < 0)
		{
			y = y + ystep;
			error += deltax;
		}
		x++;
	}
}
void drawLine(SP<SdlGraphicalInterface> sgi,Point p1, Point p2 /*, surface */)
{
	drawLine(sgi,p1.x,p1.y,p2.x,p2.y);
}
void drawFunction(
				  SP<SdlGraphicalInterface> sgi,
				  float(* fcn)(float), 
				  Point offset = Point(0,0), 
				  int divisions = 10, 
				  float startx=0, 
				  float endx=1, 
				  float xScaleFactor=1, 
				  float yScaleFactor=1
				  )
{
	std::vector<Point> d;
	for(int i = 0; i < divisions; i++)
	{
		float x = (endx-startx)*i/(float)(divisions);
		d.push_back(Point(offset.x+x*xScaleFactor,offset.y+fcn(x)*yScaleFactor));
	}
	for(int i = 0; i < d.size()-1; i++)
		drawLine(sgi,d[i],d[i+1]);
}

void drawVector(
				SP<SdlGraphicalInterface> sgi,
				std::vector<Point> pts, 
				Point offset = Point(0,0),
				float xScaleFactor=1, 
				float yScaleFactor=1,
				int startIndex = 0,
				int endIndex = -1,
				int stepSize = 1)
{
	if (endIndex == -1)
		endIndex = pts.size();
	int lastIndexDrawn = 0;
	for(int i = startIndex; i < endIndex; i+=stepSize)
	{
		drawLine(
			sgi,
			offset.x+pts[lastIndexDrawn].x*xScaleFactor,
			offset.y+pts[lastIndexDrawn].y*yScaleFactor,
			offset.x+pts[i].x*xScaleFactor,
			offset.y+pts[i].y*yScaleFactor
			);

		lastIndexDrawn = i;
	}
}
template<typename T>
void drawArray(
				SP<SdlGraphicalInterface> sgi,
				T pts, 
				int endIndex,
				int startIndex = 0,
				Point offset = Point(0,0),
				float xScaleFactor=1, 
				float yScaleFactor=1,
				int stepSize = 1)
{
	int lastIndexDrawn = startIndex;
	for(int i = startIndex+stepSize; i < endIndex; i+=stepSize)
	{
		drawLine(
			sgi,
			offset.x+(i-startIndex - stepSize)*xScaleFactor,
			offset.y+pts[lastIndexDrawn]*yScaleFactor,
			offset.x+(i-startIndex)*xScaleFactor,
			offset.y+pts[i]*yScaleFactor
			);

		lastIndexDrawn = i;
	}
}

void drawLane(SP<SdlGraphicalInterface> sgi, float phase, int ypos,float freq,float amp, int screenWidth)
{
	float  numberSegments = freq / 100.0;
	drawFunction(sgi, &cos, Point(0,ypos),500,0,numberSegments*3.1415,screenWidth/(numberSegments*3.1415),amp);
}

/*
void drawVector(
				SP<SdlGraphicalInterface> sgi,
				std::vector<Point> pts, 
				Point offset = Point(0,0),
				int minStepSize = 1,
				float xScaleFactor=1, 
				float yScaleFactor=1,
				int startIndex = 0,
				int endIndex = -1)
{
	if (endIndex == -1)
		endIndex = pts.size();

	std::vector<Point> deriv;
	for(int i = 0; i < pts.size(); i++)
	{
	}
	//calculate all derivatives


	int maxStepSize = 9;
	int lastIndexDrawn = 0;
	for(int i = 0; i < pts.size()+maxStepSize; i++)
	{
		if(i >= pts.size())
			drawLine(sgi,pts[lastIndexDrawn],pts[pts.size()-1]);


		//take up to 9 steps

		//draw line

		lastIndexDrawn = i;
	}
}
*/
