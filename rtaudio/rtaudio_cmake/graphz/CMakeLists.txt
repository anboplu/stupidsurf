# The name of our project is "GRAPHZ". CMakeLists files in this project can 
# refer to the root source directory of the project as ${GRAPHZ_SOURCE_DIR} and 
# to the root binary directory of the project as ${GRAPHZ_BINARY_DIR}. 
cmake_minimum_required (VERSION 2.4) 
project (GRAPHZ)

if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)

if(UNIX)
    include_directories (${GRAPHZ_SOURCE_DIR}/include_ln)
else(UNIX)
    include_directories (${GRAPHZ_SOURCE_DIR}/include)
endif(UNIX)

include_directories (${GRAPHZ_SOURCE_DIR}/../rtaudio_lib)

link_directories (${RTAUDIO_DX_SOURCE_DIR}/lib)
link_directories (${GRAPHZ_SOURCE_DIR})

add_executable(GRAPHZ main.cpp GuiGen.h GuiGen.cpp GuiSdl.cpp GuiSdl.h Preloader.cpp Preloader.h SmartPointer.h SmartPointer.cpp)

target_link_libraries(GRAPHZ RTAUDIO_DX SDLmain SDL SDL_image)

