#include "RtAudio.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>

#include <sndfile.hh>

#include <limits.h>

typedef double READ_UNIT;

template<class T>
T MyAbs(T t){return t >= 0 ? t : -t;}

template<class T>
struct MaxTracker
{
    bool bInit;
    T val;

    MaxTracker():bInit(false){}
    MaxTracker(T t):bInit(true), val(t){}

    void Boom(T t)
    {
        if(!bInit || t > val)
        {
            bInit = true;
            val = t;
        }
    }
};

std::vector<READ_UNIT> vRecord;

int record( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
         double streamTime, RtAudioStreamStatus status, void *userData )
{
    if ( status )
        std::cout << "Stream overflow detected!" << std::endl;

    READ_UNIT* pInputBuffer = reinterpret_cast<READ_UNIT*>(inputBuffer);

    vRecord.reserve(vRecord.size() + nBufferFrames);

    MaxTracker<READ_UNIT> tr(0);
    for(unsigned i = 0; i < nBufferFrames; ++i)
    {
        READ_UNIT f = READ_UNIT(pInputBuffer[i])/INT_MAX;
        vRecord.push_back(f);
        tr.Boom(f);
    }

    std::cout << nBufferFrames << ": " << tr.val << "         \n";

    return 0;
}

void write()
{
    const int format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
	const int channels = 1;
	const int sampleRate = 44100;
	const char* outfilename="recording.wav";

	SndfileHandle outfile(outfilename, SFM_WRITE, format, channels, sampleRate);
	if (! outfile)
        return;

    unsigned size = unsigned(vRecord.size());
	float* sample = new float[size];
    
    //std::ofstream ofs("out.txt");
    
    MaxTracker<READ_UNIT> tr(0);
    for(unsigned i = 0; i < size; ++i)
        tr.Boom(vRecord[i]);
    READ_UNIT uNorm = tr.val;
    for(unsigned i = 0; i < size; ++i)
    {
        sample[i] = vRecord[i] / uNorm;
        //ofs << vRecord[i] << "\n";
    }
	
    outfile.write(&sample[0], size);

    delete[] sample;
}

int main()
{
    RtAudio adc;
    if ( adc.getDeviceCount() < 1 ) {
        std::cout << "\nNo audio devices found!\n";
        exit( 0 );
    }

    RtAudio::StreamParameters parameters;
    parameters.deviceId = adc.getDefaultInputDevice();
    parameters.nChannels = 2;
    parameters.firstChannel = 0;
    unsigned int sampleRate = 44100;
    unsigned int bufferFrames = 1024;

    RtAudio::StreamOptions options;
    options.flags = RTAUDIO_NONINTERLEAVED;

    try {
        adc.openStream( NULL, &parameters, RTAUDIO_FLOAT64,
                        sampleRate, &bufferFrames, &record, 0, &options );
        adc.startStream();
    }
    catch ( RtError& e ) {
        e.printMessage();
        exit( 0 );
    }

    char input;
    std::cout << "\nRecording ... press <enter> to quit.\n";
    std::cin.get( input );

    try {
        // Stop the stream
        adc.stopStream();
    }
    catch (RtError& e) {
        e.printMessage();
    }

    if ( adc.isStreamOpen() ) adc.closeStream();

    write();

    return 0;
}