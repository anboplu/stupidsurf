#ifndef SURF_HEADER_08_26_10_07_25
#define SURF_HEADER_08_26_10_07_25

#include "SmartPointer.h"
#include "Preloader.h"
#include <vector>

#include "Global.h"

#include <iostream>

#include "event.h"
#include "RtAudio.h"


using namespace std;
using namespace Gui;

typedef CameraControl<Index> MyCamera;
typedef GraphicalInterface<Index> Graphic;
typedef SoundInterface<Index> SoundManager;

extern unsigned nFrameRate;

template<class T, class G = int>
struct MaxTracker
{
    bool bInit;
    T val;
    G buff;

    MaxTracker():bInit(false){}
    MaxTracker(T t, G g = G()):bInit(true), val(t), buff(g){}

    void Boom(T t, G g = G())
    {
        if(!bInit || t > val)
        {
            bInit = true;
            val = t;
            buff = g;
        }
    }

    void Reset()
    {
        bInit = false;
    }
};

struct HappyPack
{
    MyPreloader& pr;
    MyCamera cc;
    SP<SoundManager> pSn;
    SP<FontWriter> pFont;
    SP<FontWriter> pSmallFont;

    HappyPack(MyPreloader& pr_, MyCamera cc_, SP<SoundManager> pSn_, SP<FontWriter> pFont_, SP<FontWriter> pSmallFont_)
        :pr(pr_), cc(cc_), pSn(pSn_), pFont(pFont_), pSmallFont(pSmallFont_){}
};

struct Background
{
    Rectangle rBound;
    Color c;

    Background(Rectangle rBound_, Color c_ = Color())
        :rBound(rBound_), c(c_){}

    void Draw(SP<Graphic> pGr){pGr->DrawRectangle(rBound, c, false);}
};

struct GameController: virtual public SP_Info
{
    HappyPack hp;
    Background bkgr;

    GameController(HappyPack hp_, Background bkgr_):hp(hp_), bkgr(bkgr_){}

    virtual void OnKey(GuiKeyType nCode, bool bUp){};
    virtual void Update(){};

    void UpdateFull();
};

struct MenuEntry
{
    std::string strText;
    SP<Event> pClick;

    MenuEntry(std::string strText_, SP<Event> pClick_)
        :strText(strText_), pClick(pClick_){}
};

class MenuController: public GameController
{
protected:
    SP<Event> pExitEvent;
    
    std::vector<MenuEntry> vEntry;
    int nPos;

    int nOffset;

public:
    MenuController(HappyPack hp, Background bkgr, SP<Event> pExitEvent_)
        : GameController(hp, bkgr), nPos(0), pExitEvent(pExitEvent_), nOffset(0) {}

    void AddEntry(std::string str, SP<Event> pClick);

    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void Update();
};

class ScrollableMenu: public MenuController
{
protected:
    std::vector<MenuEntry> vActualEntry;
    int nActualPos;

    unsigned nLength;
public:

    ScrollableMenu(HappyPack hp, Background bkgr, SP<Event> pExitEvent, int nLength_)
        : MenuController(hp, bkgr, pExitEvent), nLength(nLength_), nActualPos(0){}

    void AddEntry(std::string str, SP<Event> pClick);

    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);

    void LoadList(int nMenuPos);
};

template<class T> 
class Matrix
{
    Size sz;
    std::vector<T> v;
public:
    Matrix(Size sz_):sz(sz_), v(sz_.x * sz_.y){}

    T& operator [](Point p){return v[p.x + p.y * sz.x];}
    T& at(Point p){return v.at(p.x + p.y * sz.x);}
};

class ControlsController: public GameController
{
    SP<Event> pTerminate;
public:

    ControlsController(HappyPack hp, Background bkgr, SP<Event> pTerminate_)
        :GameController(hp, bkgr), pTerminate(pTerminate_){}

    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void Update();
};

class CreditsController: public GameController
{
    SP<Event> pTerminate;
public:

    CreditsController(HappyPack hp, Background bkgr, SP<Event> pTerminate_)
        :GameController(hp, bkgr), pTerminate(pTerminate_){}

    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void Update();
};

class EventfulSequence
{
    std::vector< SP<Event> > vEvents;
    SP<Event> pFinalEvent;
    ImageSequence img;

    bool bFirst;

    EventfulSequence(){}
public:
    
    EventfulSequence(ImageSequence img_);

    void SetEvent(unsigned n, SP<Event> pEv);
    void SetFinalEvent(SP<Event> pEv);

    bool Toggle();
    bool ToggleTimed();
    Index GetImage();

    EventfulSequence Spawn(unsigned n);
};

class EventAnimation: public SP_Info
{
    EventfulSequence imgEvSeq;

    bool bActive;
    bool bOnce;
    bool bCentered;

    bool bTimed;
    Timer tLife;

    Timer tInterval;

public:
    SP<Event> pOnExit;

    Point pPos;

    EventAnimation(EventfulSequence imgEvSeq_, bool bCentered_, SP<Event> pOnExit_, unsigned nTime, bool bOnce_ = false);
    EventAnimation(EventfulSequence imgEvSeq_, bool bCentered_, SP<Event> pOnExit_, unsigned nTime, unsigned nLifeTime, bool bOnce_ = false);

    void Terminate();
    void Update();
    void Draw(MyCamera cc, Point p);

    void CutOff(){bOnce = true;}
};

struct Victim: virtual public SP_Info
{
    bool bExist;
    Victim():bExist(true){}

    virtual ~Victim(){}

    virtual void Update()=0;
    virtual float GetLevel() {return 10.F;}
    virtual float GetRadius(){return 0.F;}
};

class SimpleAnimator: public GameController
{
    std::list< SP<Victim> > lsVictims;
    SP<Event> pTerminateEv;
public:
    SimpleAnimator(HappyPack hp, Background bkgr, SP<Event> pTerminateEv_)
        :GameController(hp, bkgr), pTerminateEv(pTerminateEv_){}

    void NewVictim(SP<Victim> v){lsVictims.push_back(v);}

    void Terminate(){Trigger(pTerminateEv);}

    /*virtual*/ void Update();
    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
};

struct AnimationPack: public SP_Info
{
    fPoint pPos;
    fPoint pVel;
    fPoint pAcc;
    float fLevel;
    float fRadius;

    AnimationPack(fPoint pPos_, float fLevel_ = 10.F, fPoint pVel_ = fPoint(), float fRadius_ = 0.F, fPoint pAcc_ = fPoint())
        :pPos(pPos_), fLevel(fLevel_), pVel(pVel_), fRadius(fRadius_), pAcc(pAcc_){}

    virtual ~AnimationPack(){}

    virtual void Move()
    {
        pPos += pVel;
        pVel += pAcc;
    }

    Point GetPos()
    {
        return pPos.ToPnt();
    }
};

struct Animate: public Victim
{
    HappyPack hp;
    AnimationPack ap;

    EventAnimation ea;

    SP<Event> pEvExit;

    Animate(HappyPack hp_, AnimationPack ap_, EventAnimation ea_, SP<Event> pEvExit_)
        :hp(hp_), ea(ea_), ap(ap_), pEvExit(pEvExit_)
    {
        ea.pOnExit = NewTerminatorEvent(this);
    }

    /*virtual*/ void Update();

    void Terminate()
    {
        bExist = false;
        Trigger(pEvExit);
    }

    /*virtual*/ float GetLevel(){return ap.fLevel;}
    /*virtual*/ float GetRadius(){return ap.fRadius;}
};

struct Dude;

class BackToFlyEvent: public Event
{
    Dude* pDude;
public:
    BackToFlyEvent(Dude* pDude_):pDude(pDude_){}
    /*virtual*/ void Trigger();
};

struct Game;

class ShootEvent: public Event
{
    HappyPack hp;
    Dude* pDude;
    Game* pGm;
public:
    ShootEvent(HappyPack hp_, Dude* pDude_, Game* pGm_)
        :hp(hp_), pDude(pDude_), pGm(pGm_){}
    /*virtual*/ void Trigger();
};


struct Dude: public Victim
{
    HappyPack hp;
    Game* pGm;

    std::vector<EventfulSequence> vSeqs;

    SP<EventAnimation> pActive;

    Point pPos;
    Point pDiff;


    Dude(HappyPack hp_, Game* pGm_);

    void Fly();
    //void FlyReady();
    //void Hit();
    void Shoot();

    void Update();

    Point GetPosition(){return pPos;}
    virtual float GetLevel() {return 8.F;}
};

struct LevelStructure
{
    int nLevels;
    std::vector< std::vector<int> > vTicks;

    int nTickTime;
    int nVel;

    LevelStructure(){}
    LevelStructure(int nLevels_, int nTickTime_, int nVel_, int nLength)
        :nLevels(nLevels_), nTickTime(nTickTime_), nVel(nVel_), vTicks(nLength, std::vector<int>(nLevels_, 0)){}
};

std::istream& operator >> (std::istream& istr, LevelStructure& lvlStr);
std::ostream& operator << (std::ostream& ostr, const LevelStructure& lvlStr);

struct AstroGenerator: public Victim
{
    Game* pGm;

    AstroGenerator(Game* pGm_):pGm(pGm_){}
    /*virtual*/ void Update();
};

struct CowGenerator: public Victim
{
    Game* pGm;

    CowGenerator(Game* pGm_):pGm(pGm_){}
    /*virtual*/ void Update();
};

bool Collide(float fR1, Point pPos1, float fR2, Point pPos2);
bool Collide(SP<Animate> pA1, SP<Animate> pA2);

void BulletVsPig(Game* pGm, SP<Animate> pBull, SP<Animate> pPig);
void BulletVsCow(Game* pGm, SP<Animate> pBull, SP<Animate> pCow);

void BunnyVsPig(Game* pGm);

void FillInFrequencyRange(std::vector<float>& vFreqRange, float fMin, float fMax);
int InFrequencyRange(const std::vector<float>& vFreqRange, float freq);

struct ThreadCommData
{
    bool bShoot;
    float fFreq;

    float fFreqMin_Gl;
    float fFreqMax_Gl;
    int nFreqTot_Gl;

    float fSumMin;
    float fFreMin;

    bool bTrack;
    MaxTracker<float> trMaxSum;
    MaxTracker<float> trMaxWhi;

    ThreadCommData():bShoot(false), fFreq(-1), bTrack(false){}
};

enum FreqData { FD_RANGE_MIN, FD_RANGE_MAX, FD_WHISTLE_LOUDNESS_MIN, FD_SHOOT_LOUDNESS_MIN };
std::ostream& operator << (std::ostream& ofs, FreqData fd) {return ofs << int(fd);} 
std::istream& operator >> (std::istream& ifs, FreqData& fd) {int n; ifs >> n; fd = FreqData(n); return ifs;} 

struct StoredData
{
    SP<RecordCollection> pTotal;

    SP< Record<FreqData, float> > pFreqData;
};

struct Game: public SimpleAnimator
{
    LevelStructure lvlStr;
    Timer tTickTimer;
    unsigned nTick;
    
    ThreadCommData& tcd;
    StoredData sd;

    std::vector<Point> vPos;
    int nPos;

    std::vector<float> vFreqRange;

    std::list< SP<Animate> > lsBullets;
    std::list< SP<Animate> > lsPigs;
    std::list< SP<Animate> > lsCows;

    SP<Dude> pDude;

    Game(HappyPack hp, Background bkgr, SP<Event> pTerminateEv, const LevelStructure& lvlStr_, ThreadCommData& tcd_, StoredData sd_);   

    void NewCollidable(int nLevel, bool bCow);

    /*virtual*/ void OnKey(GuiKeyType nCode, bool bUp);
    /*virtual*/ void Update();
};

struct GameStarter
{
    HappyPack hp;
    Background bkgr;
    SP<Event> pTerminateEv;
    SP<GameController>& pCurrControl;
    ThreadCommData& tcd;
    StoredData sd;


    GameStarter(HappyPack hp_, Background bkgr_, SP<Event> pTerminateEv_, SP<GameController>& pCurrControl_, ThreadCommData& tcd_, StoredData sd_)
        :hp(hp_), bkgr(bkgr_), pTerminateEv(pTerminateEv_), pCurrControl(pCurrControl_), tcd(tcd_), sd(sd_){}

    SP<Game> NewGame(const LevelStructure& lvlStr)
    {
        return new Game(hp, bkgr, pTerminateEv, lvlStr, tcd, sd);
    }

    void NewGameStart(const LevelStructure& lvlStr)
    {
        pCurrControl = NewGame(lvlStr);
    }
};

class GameStartEvent: public Event
{
    GameStarter st;
    LevelStructure lvlStr;
public:

    GameStartEvent(const GameStarter& st_, const LevelStructure& lvlStr_)
        :st(st_), lvlStr(lvlStr_){}

    /*virtual*/ void Trigger(){st.NewGameStart(lvlStr);}

};

class CalibrationController: public MenuController
{
    ThreadCommData& tcd;
    StoredData sd;

    bool bReset;
public:
    CalibrationController(HappyPack hp, Background bkgr, SP<Event> pExitEvent, ThreadCommData& tcd_, StoredData sd_);

    /*virtual*/ void Update();

    void Reset();
    void Default();
    void Done();
};

void CC_Reset(CalibrationController* pCnt){pCnt->Reset();}
void CC_Default(CalibrationController* pCnt){pCnt->Default();}
void CC_Done(CalibrationController* pCnt){pCnt->Done();}

template<class A>
struct CcFnCallEvent: public Event
{
    CalibrationController* pCnt;

    A fn;

    CcFnCallEvent(CalibrationController* pCnt_, A fn_):pCnt(pCnt_), fn(fn_){}

    /*virtual*/ void Trigger(){fn(pCnt);}
};

template<class A>
SP< CcFnCallEvent<A> > NewCcFnCallEvent(CalibrationController* pCnt_, A fn_){return new CcFnCallEvent<A> (pCnt_, fn_);}

class LoudnessController: public GameController
{
    ThreadCommData& tcd;
    StoredData sd;

    SP<Event> pExitEvent;

    Timer t;

public:
    LoudnessController(HappyPack hp, Background bkgr, SP<Event> pExitEvent_, ThreadCommData& tcd_, StoredData sd_);

    void Init();

    /*virtual*/ void Update();
};

class LoundnessCalibrationEvent: public Event
{
    LoudnessController lcSample;
    SP<GameController>& pCnt;
public:
    LoundnessCalibrationEvent(const LoudnessController& lcSample_, SP<GameController>& pCnt_)
        :lcSample(lcSample_), pCnt(pCnt_){}

    /*virtual*/ void Trigger()
    {
        LoudnessController* pL = new LoudnessController(lcSample);
        pL->Init();
        pCnt = pL;
    }

};

class LevelSelect: public ScrollableMenu
{
    FilePath fp;
    GameStarter gm;
public:

    LevelSelect(HappyPack hp, Background bkgr, SP<Event> pExitEvent, int nLength,
        FilePath fp_, GameStarter gm_, std::string strPath, std::string strFile);
};

class SurfGameGlobalController: public GlobalController
{
    SP<Event> pExitProgram;

    SP<Graphic> pGr;
    SP< SoundManager > pSndMng;
    SP<MessageWriter> pMsg;

    FilePath fp;
    SP<MyPreloader> pPr;

    SP<FontWriter> pFont;
    SP<FontWriter> pSmallFont;

    SP<GameController> pCurrControl;

    SP<MenuController> pMenu;
    SP<LevelSelect> pLvlSl;
    SP<ControlsController> pCntr;
    SP<CreditsController> pCred;
    SP<MenuController> pCalChoice;
    SP<CalibrationController> pCal;
    SP<SimpleAnimator> pGenGuiLogo;

    ThreadCommData tcd;
    StoredData sd;

    RtAudio adc;
public:

    SurfGameGlobalController(ProgramEngine pe);
    ~SurfGameGlobalController();

    /*virtual*/ void Update();
    
    /*virtual*/ void KeyDown(GuiKeyType nCode);
    /*virtual*/ void KeyUp(GuiKeyType nCode);
};


#endif  //SURF_HEADER_08_26_10_07_25
