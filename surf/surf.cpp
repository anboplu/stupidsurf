#include "surf.h"

unsigned nFrameRate = 50;

typedef float READ_UNIT;

RtAudioFormat RT_READ_UNIT(){return RTAUDIO_FLOAT32;}

const unsigned sampleRate = 44100;
const unsigned bufferFrames = 1024;




READ_UNIT fourier_cos(READ_UNIT *x, int N, float frequency, int samplerate) {
    READ_UNIT fRet = 0;
    for(int i = 0; i < N; ++i)
        fRet += x[i] * cos(i * 2*3.14159265F * frequency/samplerate);
    return fRet / N * 2;
}

READ_UNIT fourier_sin(READ_UNIT *x, int N, float frequency, int samplerate) {
    READ_UNIT fRet = 0;
    for(int i = 0; i < N; ++i)
        fRet += x[i] * sin(i * 2*3.14159265F * frequency/samplerate);
    return fRet / N * 2;
}

const float frMiddleC = 261.626F;

float GetFrequency(int n)
{
    return frMiddleC * std::pow(2, float(n)/12); 
}

const int nFreqTotDef = 50;

//float fPrevFreq = -1;
//int nFreqCounter = 0;


int record( void* outputBuffer, void* inputBuffer, unsigned int nBufferFrames,
         double streamTime, RtAudioStreamStatus status, void* userData )
{
    if ( status )
        std::cout << "Stream overflow detected!" << std::endl;

    READ_UNIT* pInputBuffer = reinterpret_cast<READ_UNIT*>(inputBuffer);

    float fSum = 0;
    MaxTracker<float, float> frtr;

    ThreadCommData* pTcd = reinterpret_cast<ThreadCommData*>(userData);

    float fFreqMin = pTcd->fFreqMin_Gl;
    float fFreqMax = pTcd->fFreqMax_Gl;
    int nFreqTot = pTcd->nFreqTot_Gl;

    if(fFreqMin > fFreqMax || nFreqTot <= 0 || nFreqTot > 1000)
        return 0;
    
    float fFreqStep = (fFreqMax - fFreqMin)/nFreqTot;

    for(float freq = fFreqMin; freq <= fFreqMax; freq += fFreqStep)
    {
        float f1 = fourier_cos(pInputBuffer, nBufferFrames, freq, sampleRate);
        float f2 = fourier_sin(pInputBuffer, nBufferFrames, freq, sampleRate);
        float f = sqrt(f1*f1 + f2*f2);
        frtr.Boom(f, freq); 
    }

    for(float freq = 10; freq <= 200; freq += 10)
    {
        float f1 = fourier_cos(pInputBuffer, nBufferFrames, freq, sampleRate);
        float f2 = fourier_sin(pInputBuffer, nBufferFrames, freq, sampleRate);
        float f = sqrt(f1*f1 + f2*f2);
        fSum += f;
    }


    if(pTcd->bTrack)
    {
        pTcd->trMaxSum.Boom(fSum);
        pTcd->trMaxWhi.Boom(frtr.val);
    }
    
    if(fSum > pTcd->fSumMin)
        pTcd->bShoot = true;
    else if (frtr.val > pTcd->fFreMin)
    {
        pTcd->fFreq = frtr.buff;

        /*
        if(fPrevFreq != frtr.buff)
        {
            fPrevFreq = frtr.buff;
            nFreqCounter = 0;
        }
        else
            ++nFreqCounter;
            */
    }
    /*
    else
    {
        nFreqCounter = 0;
    }
    */

    //std::cout << "Data: " << fSum << "\t" << pTcd->trMaxSum.val << "         \r";//<< "\tAmpl: " << frtr.val << "\n";

    return 0;
}

void GameController::UpdateFull()
{
    bkgr.Draw(hp.cc.pGr);
    Update();
    hp.cc.pGr->RefreshAll();
}


void MenuController::AddEntry(std::string str, SP<Event> pClick)
{
    vEntry.push_back(MenuEntry(str, pClick));
    nOffset =  - hp.cc.fromR(Point(hp.pFont->GetSize(vEntry[0].strText).x/2, 0)).x;
}

/*virtual*/ void MenuController::OnKey(GuiKeyType nCode, bool bUp)
{
    if(vEntry.empty())
        return;

    if(bUp)
        return;

    if(nCode == GUI_UP)
    {
        --nPos;
        if(nPos < 0)
            nPos = vEntry.size() - 1;
    }
    else if(nCode == GUI_DOWN)
    {
        ++nPos;
        if(unsigned(nPos) >= vEntry.size())
            nPos = 0;
    }
    else if(nCode == GUI_RETURN)
    {
        Trigger(vEntry.at(nPos).pClick);
    }
    else if(nCode == GUI_ESCAPE)
    {
        Trigger(pExitEvent);
    }
}

/*virtual*/ void MenuController::Update()
{
    if(vEntry.empty())
        return;

    MyCamera cc = hp.cc;
    SP<FontWriter> pFnt = hp.pFont;

    cc.Push();

    cc.Translate(fPoint(.5, .5));
    Size szWord = cc.fromR(pFnt->GetSize(vEntry[0].strText));
    cc.Translate(Point(nOffset, - szWord.y * int(vEntry.size()) / 2));

    for(unsigned i = 0; i < vEntry.size(); ++i)
    {
        pFnt->DrawWord(vEntry[i].strText, cc.toR(Point()));

        if(i == nPos)
        {
            Size sz = cc.fromR(cc.pGr->GetImage(hp.pr["pointer"])->GetSize());
            cc.DrawImage(Point( - sz.x * 3 / 2, 0 ) , hp.pr["pointer"]);
        }

        cc.Translate(Point(0, szWord.y));
    }

    cc.Pop();
}

void ScrollableMenu::AddEntry(std::string str, SP<Event> pClick)
{
    vActualEntry.push_back(MenuEntry(str, pClick));
    nOffset =  - hp.pFont->GetSize(vActualEntry[0].strText).x/2;

    LoadList(0);
}

void ScrollableMenu::OnKey(GuiKeyType nCode, bool bUp)
{
    if(vActualEntry.empty())
        return;

    if(bUp)
        return;

    if(nCode == GUI_UP)
    {
        --nActualPos;
        --nPos;
        if(nActualPos < 0)
            nActualPos = vActualEntry.size() - 1;
    }
    else if(nCode == GUI_DOWN)
    {
        ++nActualPos;
        ++nPos;
        if(unsigned(nActualPos) >= vActualEntry.size())
            nActualPos = 0;
    }
    else if(nCode == GUI_RETURN)
    {
        Trigger(vActualEntry.at(nActualPos).pClick);
    }
    else if(nCode == GUI_ESCAPE)
    {
        Trigger(pExitEvent);
    }

    if(vActualEntry.size() <= nLength)
    {
        nPos = nActualPos;
    }
    else if(nActualPos == 0)
    {
        nPos = 0;
        LoadList(0);
    }
    else if(unsigned(nActualPos) == vActualEntry.size() - 1)
    {
        nPos = nLength - 1;
        LoadList(nActualPos - nLength + 1);
    }
    else if(nPos == 0)
    {
        nPos = 1;
        LoadList(nActualPos - 1);
    }
    else if(nPos == nLength - 1)
    {
        nPos = nLength - 2;
        LoadList(nActualPos - nLength + 2);
    }
}

void ScrollableMenu::LoadList(int nMenuPos)
{
    if(nMenuPos < 0)
        return;

    for(unsigned i = 0; (i < nLength) && (i + nMenuPos < vActualEntry.size()); ++i)
    {
        if(i < vEntry.size())
            vEntry[i] = vActualEntry[i + nMenuPos];
        else
            vEntry.push_back(vActualEntry[i + nMenuPos]);
    }
}
CalibrationController::CalibrationController(HappyPack hp, Background bkgr, SP<Event> pExitEvent, ThreadCommData& tcd_, StoredData sd_)
:MenuController(hp, bkgr, pExitEvent), tcd(tcd_), sd(sd_), bReset(false)
{
    pExitEvent = new SequenceOfEvents( NewCcFnCallEvent(this, &CC_Done), pExitEvent);
    
    AddEntry("DEFAULT", NewCcFnCallEvent(this, &CC_Default));
    AddEntry("RESET", NewCcFnCallEvent(this, &CC_Reset));
    AddEntry("DONE", pExitEvent);
}

void CalibrationController::Update()
{
    MenuController::Update();

    if(tcd.nFreqTot_Gl != 100)
    {
        tcd.fFreqMin_Gl = 500;
        tcd.fFreqMax_Gl = 3000;
        tcd.nFreqTot_Gl = 100;
    }

    float fS = .1F;

    Point p1 = hp.cc.fromF(fPoint(fS, .25));
    Point p2 = hp.cc.fromF(fPoint(1 - fS, .25));

    p2.y += 1;
    
    hp.cc.DrawRectangle(Rectangle(p1, p2), Color(0,0,0));

    float& fRangeMin = sd.pFreqData->Get(FD_RANGE_MIN);
    float& fRangeMax = sd.pFreqData->Get(FD_RANGE_MAX);

    if(!bReset)
    {
        std::string sRangeMsg = "RANGE " + S(int(fRangeMin)) + " - " + S(int(fRangeMax));

        hp.pFont->DrawWord(sRangeMsg, hp.cc.toR(hp.cc.fromF(fPoint(.5, .1))), true);
    }
    
    float f = tcd.fFreq;

    if(f < 0)
        return;
    
    std::string s = S(int(f)) + " HZ";

    if(bReset)
    {
        fRangeMin = f;
        fRangeMax = f;
        bReset = false;
    }
    else 
    {
        if(f < fRangeMin)
            fRangeMin = f;

        if(f > fRangeMax)
            fRangeMax = f;
    }

    
    float fPos;

    if(f < 1600)
        fPos = 2 * f / 1600;
    else
        fPos = 3 - 1600 / f;

    fPos /= 3;

    fPos = fS + fPos * (1 - 2 * fS);

    hp.cc.Push();
    hp.cc.Translate(fPoint(fPos, .25));
    Point p(4,5);
    hp.cc.DrawRectangle(Rectangle(-p, p), Color(0,0,0));
    p = Point(1,4);
    hp.cc.DrawRectangle(Rectangle(-p, p), Color(255,255,255));

    Size sz = hp.cc.fromR(hp.pSmallFont->GetSize(s));

    hp.cc.Translate(Point(0, - sz.y - 4));

    hp.pSmallFont->DrawWord(s, hp.cc.toR(Point()), true);

    hp.cc.Pop();
}

void CalibrationController::Reset()
{
    bReset = true;

    tcd.fFreq = -1;
}

void CalibrationController::Default()
{
    sd.pFreqData->Get(FD_RANGE_MIN) = 800;
    sd.pFreqData->Get(FD_RANGE_MAX) = 1200;

    tcd.fFreq = -1;
}

void CalibrationController::Done()
{
    tcd.fFreqMin_Gl = sd.pFreqData->Get(FD_RANGE_MIN);
    tcd.fFreqMax_Gl = sd.pFreqData->Get(FD_RANGE_MAX);
    tcd.nFreqTot_Gl = nFreqTotDef;

    sd.pTotal->WriteDef();
}

LoudnessController::LoudnessController(HappyPack hp, Background bkgr, SP<Event> pExitEvent_, ThreadCommData& tcd_, StoredData sd_)
:GameController(hp, bkgr), pExitEvent(pExitEvent_), tcd(tcd_), sd(sd_), t(100)
{
}

void LoudnessController::Init()
{
    tcd.fFreqMin_Gl = 500;
    tcd.fFreqMax_Gl = 3000;
    tcd.nFreqTot_Gl = 100;

    tcd.trMaxSum.Reset();
    tcd.trMaxWhi.Reset();
    tcd.bTrack = true;
}

void DrawMeter(MyCamera& cc, float x, float y1, float y2, float fPos)
{
    float y = y1 + (y2 - y1)*(1 - fPos);

    fPoint f1(x, y1);
    fPoint f2(x, y2);

    Point p1 = cc.fromF(f1);
    Point p2 = cc.fromF(f2);

    --p1.x;
    ++p2.x;
    
    Gui::Rectangle rLine(p1, p2);
    Gui::Rectangle rMeter   (-3, -6, 3, 6);
    Gui::Rectangle rMeterTop(-2, -5, 2, 5);

    cc.DrawRectangle(rLine, Color(0,0,0));

    cc.Push();

    cc.Translate(fPoint(x, y));

    cc.DrawRectangle(rMeter, Color(0,0,0));
    cc.DrawRectangle(rMeterTop, Color(255,255,255));
    
    cc.Pop();
}

void LoudCut(float& f)
{
    if(f <= 0)
        f = -2;
    else
        f = log(f)/log(10.F);
    
    if(f < -2)
        f = -2;

    f = (2 + f)/5;
}

void LoudnessController::Update()
{
    hp.cc.Push();

    hp.cc.Translate(fPoint(.5, .5));

    hp.pFont->DrawWord("BE AS QUIET AS POSSIBLE", hp.cc.toR(Point()), true);

    hp.cc.Pop();

    float fSum = tcd.trMaxSum.val;
    float fWhi = tcd.trMaxWhi.val;

    LoudCut(fSum);
    LoudCut(fWhi);
    
    DrawMeter(hp.cc, .1F, .1F, .9F, fSum);
    DrawMeter(hp.cc, .2F, .1F, .9F, fWhi);

    if(t.Tick())
    {
        tcd.bTrack = false;
        sd.pFreqData->Put(FD_WHISTLE_LOUDNESS_MIN, tcd.trMaxWhi.val);
        sd.pFreqData->Put(FD_SHOOT_LOUDNESS_MIN, tcd.trMaxSum.val);

        sd.pTotal->WriteDef();

        tcd.fFreMin = sd.pFreqData->Get(FD_WHISTLE_LOUDNESS_MIN);
        tcd.fSumMin = sd.pFreqData->Get(FD_SHOOT_LOUDNESS_MIN);

        tcd.fFreqMin_Gl = sd.pFreqData->Get(FD_RANGE_MIN);
        tcd.fFreqMax_Gl = sd.pFreqData->Get(FD_RANGE_MAX);
        tcd.nFreqTot_Gl = nFreqTotDef;

        Trigger(pExitEvent);
    }
}



LevelSelect::LevelSelect(HappyPack hp, Background bkgr, SP<Event> pExitEvent, int nLength,
    FilePath fp_, GameStarter gm_, std::string strPath, std::string strFile)
    :ScrollableMenu(hp, bkgr, pExitEvent, nLength), fp(fp_), gm(gm_)
{
    strFile = strPath + strFile;
    
    fp.Parse(strFile);

    std::ifstream ifs(strFile.c_str());

    if(ifs.fail())
        throw SimpleException("LevelSelect", "LevelSelect", "Cannot open " + strFile);

    while(true)
    {
        char c = ifs.get();

        if(ifs.fail())
            break;

        if(c == '\n')
            continue;
        
        ifs.putback(c);

        std::string str;
        std::getline(ifs, str);

        std::string str_path = fp.GetParse(strPath + str);
        str_path += ".txt";

        std::ifstream ifs_lvl(str_path.c_str());

        if(ifs_lvl.fail())
        {
            std::cout << "Warning: cannot find " << str << "\n";
            continue;
        }

        LevelStructure lvl;
        ifs_lvl >> lvl;

        if(ifs_lvl.fail())
        {
            std::cout << "Warning: cannot read " << str << "\n";
            continue;
        }

        for(unsigned i = 0; i < str.length(); ++i)
            str[i] = toupper(str[i]);

        AddEntry(str, new GameStartEvent(gm, lvl));
    }
}



/*virtual*/ void ControlsController::OnKey(GuiKeyType nCode, bool bUp)
{
    if(!bUp)
        Trigger(pTerminate);
}

/*virtual*/ void ControlsController::Update()
{
    MyCamera cc = hp.cc;
    SP<FontWriter> pFnt = hp.pFont;

    cc.Push();

    cc.Translate(fPoint(.5, .5));
    
    std::vector<std::string> vText;

    vText.push_back("CONTROLS");
    vText.push_back("WHISTLE TO MOVE UP OR DOWN");
    vText.push_back("CLAP TP SHOOT");

    Size sz = cc.fromR(pFnt->szSymbol);
    int nHeight = vText.size() * sz.y;
    cc.Translate(Point(0, -nHeight/2));

    for(unsigned i = 0; i < vText.size(); ++i)
    {
        pFnt->DrawWord(vText[i], cc.toR(Point()), true, false);
        cc.Translate(Point(0, sz.y));
    }

    cc.Pop();
}

/*virtual*/ void CreditsController::OnKey(GuiKeyType nCode, bool bUp)
{
    if(!bUp)
        Trigger(pTerminate);
}

/*virtual*/ void CreditsController::Update()
{
    MyCamera cc = hp.cc;
    SP<FontWriter> pFnt = hp.pFont;
    
    cc.Push();

    cc.Translate(fPoint(.5, .5));
    
    std::vector<std::string> vText;

    vText.push_back("CREDITS");
    vText.push_back("ANTON BOBKOV");
    vText.push_back("PETER LU");

    Size sz = cc.fromR(pFnt->szSymbol);
    int nHeight = vText.size() * sz.y;
    cc.Translate(Point(0, -nHeight/2));

    for(unsigned i = 0; i < vText.size(); ++i)
    {
        pFnt->DrawWord(vText[i], cc.toR(Point()), true, false);
        cc.Translate(Point(0, sz.y));
    }

    cc.Pop();
}

EventfulSequence::EventfulSequence(ImageSequence img_)
:img(img_), vEvents(img_.vImage.size(), 0), bFirst(true)
{}

void EventfulSequence::SetEvent(unsigned n, SP<Event> pEv)
{
    vEvents.at(n) = pEv;
}

void EventfulSequence::SetFinalEvent(SP<Event> pEv)
{
    pFinalEvent = pEv;
}

bool EventfulSequence::Toggle()
{
    if(bFirst)
    {
        bFirst = false;
        Trigger(vEvents.at(img.nActive));
    }
    
    bool b = img.Toggle();
    if(b)
        Trigger(pFinalEvent);
    Trigger(vEvents.at(img.nActive));
    return b;
}

bool EventfulSequence::ToggleTimed()
{
    if(bFirst)
    {
        bFirst = false;
        Trigger(vEvents.at(img.nActive));
    }

    int nBuff = img.nActive;
    bool b = img.ToggleTimed();
    if(b)
        Trigger(pFinalEvent);
    if(nBuff != img.nActive)
        Trigger(vEvents.at(img.nActive));
    return b;
}

Index EventfulSequence::GetImage()
{
    return img.GetImage();
}

EventfulSequence EventfulSequence::Spawn(unsigned n)
{
    EventfulSequence eRet;
    eRet.pFinalEvent = pFinalEvent;

    unsigned sz = vEvents.size();
    for(unsigned i = 0; i < sz*n; ++i)
    {
        eRet.img.Add(img.vImage.at(i%sz), img.vIntervals.at(i%sz));
        eRet.vEvents.push_back(vEvents.at(i%sz));
    }

    return eRet;
}



EventAnimation::EventAnimation(EventfulSequence imgEvSeq_, bool bCentered_, SP<Event> pOnExit_, unsigned nTime, bool bOnce_ )
:imgEvSeq(imgEvSeq_), bCentered(bCentered_), pOnExit(pOnExit_), bOnce(bOnce_), bTimed(false), bActive(true), tInterval(nTime){}

EventAnimation::EventAnimation(EventfulSequence imgEvSeq_, bool bCentered_, SP<Event> pOnExit_, unsigned nTime, unsigned nLifeTime, bool bOnce_)
:imgEvSeq(imgEvSeq_), bCentered(bCentered_), pOnExit(pOnExit_), bOnce(bOnce_), bTimed(true), tLife(nLifeTime), bActive(true), tInterval(nTime){}

void EventAnimation::Terminate()
{
    bActive = false;
    Trigger(pOnExit);
}

void EventAnimation::Update()
{
    if(!bActive)
        return;

    if(tInterval.Tick())
        if(imgEvSeq.ToggleTimed() && bOnce)
        {
            Terminate();
            return;
        }

    if(bTimed && tLife.Tick())
    {
        Terminate();
        return;
    }
}

void EventAnimation::Draw(MyCamera cc, Point p)
{
    if(!bActive)
        return;

    cc.DrawImage(p, imgEvSeq.GetImage(), bCentered, false);
}

template<class T>
void Sanitize(std::list< T >& ls)
{
    for(typename std::list< T >::iterator itr = ls.begin(), etr = ls.end();
        itr != etr;)
        if(!(*itr)->bExist)
            ls.erase(itr++);
        else
            ++itr;
}



void BackToFlyEvent::Trigger()
{
    pDude->Fly();
}

void Animate::Update()
{
    if(!bExist)
        return;
    
    ea.Update();
    ea.Draw(hp.cc, ap.GetPos());
    ap.Move();

    
    Rectangle r(hp.cc.GetBox());
    int nR = int(GetRadius());
    Point p(nR, nR);
    r = Rectangle(r.p - p, r.GetBottomRight() + p);
    if(!InsideRectangle(r, ap.GetPos()))
        Terminate();
}

void SimpleAnimator::Update()
{
    std::multimap<float, SP<Victim> > mmmp;

    for(std::list< SP<Victim> >::iterator itr = lsVictims.begin(), etr = lsVictims.end(); itr != etr; ++itr)
        mmmp.insert(std::pair<float, SP<Victim> >((*itr)->GetLevel(), (*itr)));

    for(std::multimap<float, SP<Victim> >::iterator itr = mmmp.begin(), etr = mmmp.end(); itr != etr; ++itr)
        if(itr->second->bExist)
            itr->second->Update();

    Sanitize(lsVictims);
}

void SimpleAnimator::OnKey(GuiKeyType nCode, bool bUp)
{
    if(!bUp)
        Terminate();
}



Dude::Dude(HappyPack hp_, Game* pGm_)
:hp(hp_), pGm(pGm_)
{
    vSeqs.push_back(hp.pr("fly"));
    vSeqs.back().SetEvent(1, new MakeSoundEvent(hp.pSn, hp.pr.GetSnd("fly")));

    Size sz = hp.cc.fromR(hp.cc.pGr->GetImage(vSeqs.back().GetImage())->GetSize());
    pDiff = - Point(sz.x/2, sz.y/2);

    //vSeqs.push_back(hp.pr("hit"));

    vSeqs.push_back(hp.pr("shoot"));
    vSeqs.back().SetEvent(0, new ShootEvent(hp, this, pGm));

    Fly();

}


void Dude::Fly()
{
    pActive = new EventAnimation(vSeqs.at(0), false, 0, 2);
}

/*
void Dude::FlyReady()
{
    pActive->CutOff();
}

void Dude::Hit()
{
    hp.pSn->PlaySound(hp.pr.GetSnd("hit"));
    
    EventfulSequence mult = vSeqs.at(1).Spawn(5);
    pActive = new EventAnimation(mult, false, new BackToFlyEvent(this), 1, true);
}
*/

void Dude::Shoot()
{
    pActive = new EventAnimation(vSeqs.at(1), false, new BackToFlyEvent(this), 2, true);
}

void Dude::Update()
{
    pActive->Draw(hp.cc, pPos + pDiff);
    pActive->Update();
}

Game::Game(HappyPack hp, Background bkgr, SP<Event> pTerminateEv, const LevelStructure& lvlStr_, ThreadCommData& tcd_, StoredData sd_)
:SimpleAnimator(hp, bkgr, pTerminateEv), nPos(0), sd(sd_),
lvlStr(lvlStr_), nTick(0), tTickTimer(lvlStr_.nTickTime), tcd(tcd_)
{
    vFreqRange.resize(lvlStr.nLevels + 1);
    FillInFrequencyRange(vFreqRange, sd.pFreqData->Get(FD_RANGE_MIN), sd.pFreqData->Get(FD_RANGE_MAX));
    
    float f = 1.F/(lvlStr.nLevels + 1);
    
    for(int i = 0; i < lvlStr.nLevels; ++i)
        vPos.push_back(hp.cc.fromF(fPoint(.1, f * (i + 1))));
    
    //vPos.push_back(hp.cc.fromF(fPoint(.1, .25)));
    //vPos.push_back(hp.cc.fromF(fPoint(.1, .5)));
    //vPos.push_back(hp.cc.fromF(fPoint(.1, .75)));
    
    pDude = new Dude(hp, this);
    NewVictim(pDude);

    //NewVictim(new AstroGenerator(this));
    //NewVictim(new   CowGenerator(this));
}

void Game::NewCollidable(int nLevel, bool bCow)
{    
    EventAnimation eaCow(hp.pr(bCow ? "cow" : "pig"), true, 0, 2);
    Point p = hp.cc.fromF(fPoint(.9, 0));
    p.y = vPos.at(nLevel).y;
    SP<Animate> pCow = new Animate(hp, AnimationPack(p, 6.F, fPoint(-lvlStr.nVel, 0), 35.F), eaCow, 0);
    NewVictim(pCow);
    if(bCow)
        lsCows.push_back(pCow);
    else
        lsPigs.push_back(pCow);
}



void Game::OnKey(GuiKeyType nCode, bool bUp)
{
    if(!bUp)
    {
        if(nCode == GUI_ESCAPE)
            Terminate();
        //else if(nCode == 'z')
        //    pDude->Hit();
        else if(nCode == 'x')
            pDude->Shoot();
        else if(nCode == GUI_DOWN)
        {
            if(nPos != vPos.size() - 1)
                ++nPos;
        }
        else if(nCode == GUI_UP)
        {
            if(nPos != 0)
                --nPos;
        }
    }
    else
    {
        //if(nCode == 'x')
        //    pDude->FlyReady();
    }
}

bool Collide(float fR1, Point pPos1, float fR2, Point pPos2)
{
    return (fR1 + fR2) > fPoint(pPos1 - pPos2).Length();
}

bool Collide(SP<Animate> pA1, SP<Animate> pA2)
{
    return Collide(pA1->GetRadius(), pA1->ap.GetPos(), pA2->GetRadius(), pA2->ap.GetPos());
}

void BulletVsPig(Game* pGm, SP<Animate> pBull, SP<Animate> pPig)
{
    pBull->bExist = false;
    pPig->bExist = false;

    EventAnimation ea(pGm->hp.pr("dead_pig"), true, 0, 1, false);
    SP<Animate> pDeath = new Animate(pGm->hp,
        AnimationPack(pPig->ap.GetPos(), 10.F, fPoint(5, 0), 0, fPoint(0, 5)),
        ea, 0);
    pGm->NewVictim(pDeath);
}

void BulletVsCow(Game* pGm, SP<Animate> pBull, SP<Animate> pCow)
{
    pBull->bExist = false;
}

void FillInFrequencyRange(std::vector<float>& vFreqRange, float fMin, float fMax)
{
    float fStep = (fMax - fMin)/(vFreqRange.size() - 1);

    for(unsigned i = 0; i < vFreqRange.size(); ++i)
        vFreqRange[i] = fMin + fStep * i;
}

int InFrequencyRange(const std::vector<float>& vFreqRange, float freq)
{
    for(int i = 0; i < int(vFreqRange.size()); ++i)
        if(freq < vFreqRange[i])
            return i - 1;
    return -1;
}


void BunnyVsPig(Game* pGm)
{
    pGm->pDude->bExist = false;

    EventAnimation ea(pGm->hp.pr("bunny_die"), true, 0, 1, false);
    SP<Animate> pDeath = new Animate(pGm->hp,
        AnimationPack(pGm->pDude->GetPosition(), 10.F, fPoint(-5, -10), 0, fPoint(0, 5)),
        ea, NewTerminatorEvent(pGm) );
    pGm->NewVictim(pDeath);
}



void BulletCheck(Game* pGm, SP<Animate> pBull)
{
    for(std::list< SP<Animate> >::iterator itr = pGm->lsPigs.begin(), etr = pGm->lsPigs.end(); itr != etr; ++itr)
        if(Collide(pBull, *itr))
            BulletVsPig(pGm, pBull, *itr);

    for(std::list< SP<Animate> >::iterator itr = pGm->lsCows.begin(), etr = pGm->lsCows.end(); itr != etr; ++itr)
        if(Collide(pBull, *itr))
            BulletVsCow(pGm, pBull, *itr);
}


/*virtual*/ void Game::Update()
{
    if(tcd.bShoot)
    {
        tcd.bShoot = false;
        pDude->Shoot();
    }

    int nRes = InFrequencyRange(vFreqRange, tcd.fFreq);

    if(nRes != -1)
    {
        //if(nPos != nRes)
        //    std::cout << nRes << "\n";
        nPos = nRes;
    }
    
    if(tTickTimer.Tick())
    {
        if(nTick < lvlStr.vTicks.size())
        {
            for(int i = 0; i < lvlStr.nLevels; ++i)
            {
                if(lvlStr.vTicks[nTick][i] == 0)
                    continue;
                
                NewCollidable(i, lvlStr.vTicks[nTick][i] == 1);
            }
        }
        else if(lsPigs.empty() && lsCows.empty() && pDude->bExist)
            Terminate();
        
        ++nTick;
    }

    pDude->pPos = vPos.at(nPos);


    for(std::list< SP<Animate> >::iterator itr = lsBullets.begin(), etr = lsBullets.end(); itr != etr; ++itr)
        BulletCheck(this, *itr);

    if(pDude->bExist)
    {
        for(std::list< SP<Animate> >::iterator itr = lsPigs.begin(), etr = lsPigs.end(); itr != etr; ++itr)
            if(Collide(pDude->GetRadius(), pDude->GetPosition(), (*itr)->GetRadius(), (*itr)->ap.GetPos()))
                BunnyVsPig(this);

        for(std::list< SP<Animate> >::iterator itr = lsCows.begin(), etr = lsCows.end(); itr != etr; ++itr)
            if(Collide(pDude->GetRadius(), pDude->GetPosition(), (*itr)->GetRadius(), (*itr)->ap.GetPos()))
                BunnyVsPig(this);
    }


    Sanitize(lsBullets);
    Sanitize(lsPigs);
    Sanitize(lsCows);

    SimpleAnimator::Update();
}



void ShootEvent::Trigger()
{
    hp.pSn->PlaySound(hp.pr.GetSnd("shoot"));

    EventAnimation eaBull(hp.pr("bullet"), true, 0, 1);
    Point p = pDude->GetPosition();
    p += Point(35, -17);
    SP<Animate> pBullet = new Animate(hp, AnimationPack(p, 4.F, fPoint(pGm->lvlStr.nVel, 0), 5.F), eaBull, 0);
    pGm->NewVictim(pBullet);
    pGm->lsBullets.push_back(pBullet);
}

std::istream& operator >> (std::istream& istr, LevelStructure& lvlStr)
{
    unsigned nSz;
    istr >> lvlStr.nTickTime >> lvlStr.nVel >> lvlStr.nLevels >> nSz;

    lvlStr.vTicks.clear();
    lvlStr.vTicks.resize(nSz, std::vector<int>(lvlStr.nLevels, 0));

    for(unsigned i = 0; i < nSz; ++i)
        for(int j = 0; j < lvlStr.nLevels; ++j)
            istr >> lvlStr.vTicks[i][j];
    
    return istr;
}

std::ostream& operator << (std::ostream& ostr, const LevelStructure& lvlStr)
{
    ostr << lvlStr.nTickTime << lvlStr.nVel << lvlStr.nLevels << lvlStr.vTicks.size() << "\n";
    for(size_t i = 0, sz = lvlStr.vTicks.size(); i < sz; ++i)
    {
        for(int j = 0; j < lvlStr.nLevels; ++j)
            ostr << lvlStr.vTicks[i][j] << " ";
        ostr << "\n";
    }

    return ostr;
}


void AstroGenerator::Update()
{
    if(rand()%50)
        return;

    MyPreloader& pr = pGm->hp.pr;
    MyCamera cc = pGm->hp.cc;
    
    EventAnimation eaAstr(pr("pig"), true, 0, 2);
    Point p = cc.fromF(fPoint(.9, 0));
    p.y = pGm->vPos[rand()%pGm->vPos.size()].y;
    SP<Animate> pAstro = new Animate(pGm->hp, AnimationPack(p, 6.F, fPoint(-5, 0), 35.F), eaAstr, 0);
    pGm->NewVictim(pAstro);
    pGm->lsPigs.push_back(pAstro);
}

void CowGenerator::Update()
{
    if(rand()%50)
        return;

    MyPreloader& pr = pGm->hp.pr;
    MyCamera cc = pGm->hp.cc;
    
    EventAnimation eaCow(pr("cow"), true, 0, 2);
    Point p = cc.fromF(fPoint(.9, 0));
    p.y = pGm->vPos[rand()%pGm->vPos.size()].y;
    SP<Animate> pCow = new Animate(pGm->hp, AnimationPack(p, 6.F, fPoint(-5, 0), 35.F), eaCow, 0);
    pGm->NewVictim(pCow);
    pGm->lsCows.push_back(pCow);
}

ProgramInfo GetProgramInfo()
{
    ProgramInfo inf;
    
    inf.szScreenRez = Size(960, 640);
    inf.strTitle = "Audio Farm";
    inf.nFramerate = nFrameRate;
    
    return inf;
}

SP<GlobalController> GetGlobalController(ProgramEngine pe)
{
    return new SurfGameGlobalController(pe);
}

SurfGameGlobalController::SurfGameGlobalController(ProgramEngine pe)
{
    RtAudio::StreamParameters parameters;
    RtAudio::StreamOptions options;
    unsigned myBufferFrames = bufferFrames;


    try
    {
        if (adc.getDeviceCount() < 1)
                throw SimpleException("SurfGameGlobalController", "<constructor>", "No working microphone found");

        parameters.deviceId = adc.getDefaultInputDevice();
        parameters.nChannels = 2;
        parameters.firstChannel = 0;

        options.flags = RTAUDIO_NONINTERLEAVED;
    }
    catch(RtError& e)
    {
        throw SimpleException("SurfGameGlobalController", "<constructor>",
            "Audio error: " + e.getMessage());
    }


    ProgramInfo inf = GetProgramInfo();
    
    Rectangle sBound = Rectangle(inf.szScreenRez);
	unsigned nScale = 1;
    Rectangle rBound = Rectangle(0, 0, sBound.sz.x/nScale, sBound.sz.y/nScale);

    pGr = pe.pGr;

    pMsg = pe.pMsg;

    FilePath fp;

    {
        std::ifstream ifs("config.txt");

        if(ifs.fail())
            throw SimpleException("SurfGameGlobalController", "<constructor>", "Need config.txt file!");

        ifs >> fp;
    }
    

    pSndMng = pe.pSndMng;

    pPr = new MyPreloader(pGr, pSndMng, fp);
    MyPreloader& pr = *pPr;

    pr.AddTransparent(Color(0,0,0));
    pr.SetScale(nScale);
    
    pr.LoadTS("pointer.bmp", "pointer", Color(255,255,255), nScale);
    pr.LoadTS("logo/anboplu.bmp", "anboplu");
    
    pr.LoadSeqTS("logo/gengui.txt", "gengui", Color(0,0,0), nScale*4);
    
    pr.LoadSnd("pistol.wav", "shoot");		       
    pr.LoadSnd("whoosh.wav", "fly");		       
    pr.LoadSnd("buzz.wav", "hit");		       
    
    pr.LoadSnd("sound/future_click.wav", "click");		       

    pr.LoadSeqTS("carrot/carrot.txt", "bullet", Color(0,255,0), nScale);
    pr.LoadSeqTS("pig/pig.txt", "pig", Color(255,0,127), nScale);
    pr.LoadSeqTS("cow/cow.txt", "cow", Color(255,255,0), nScale);
    pr.LoadSeqTS("pig/dead_pig.txt", "dead_pig", Color(0,255,255), nScale);
    pr.LoadSeqTS("bunny/fly.txt", "fly", Color(0,255,0), nScale);
    pr.LoadSeqTS("bunny/dead_bunny.txt", "bunny_die", Color(0,255,0), nScale);
    pr.LoadSeqTS("bunny/shoot.txt", "shoot", Color(0,255,0), nScale);
    
    ForEachImage(pr("fly"), ImageFlipper(pGr));
    ForEachImage(pr("shoot"), ImageFlipper(pGr));
    ForEachImage(pr("bullet"), ImageFlipper(pGr));

    MyCamera cc(pGr, Scale(float(nScale), rBound));
    cc.Zoom(2);
    MyCamera cc_reg(pGr, Scale(float(nScale), rBound));

    pFont = new FontWriter(fp, "bunnyfont/bunnyfont.txt", pGr, nScale);
    pSmallFont = pFont;

    HappyPack hp(pr, cc, pSndMng, pFont, pSmallFont);
    HappyPack hp_reg(pr, cc_reg, pSndMng, pFont, pSmallFont);
    
    pFont->SetGap(- pFont->szSymbol.x/3);
    pFont->Recolor(Color(0, 0, 255));

    sd.pTotal = new RecordCollection(fp.GetParse("records.txt"));

    sd.pFreqData = new Record<FreqData, float> ();

    sd.pTotal->NewRecordKeeper(sd.pFreqData);

    sd.pTotal->ReadDef();

    tcd.nFreqTot_Gl = nFreqTotDef;

    tcd.fFreqMin_Gl = sd.pFreqData->Get(FD_RANGE_MIN);
    tcd.fFreqMax_Gl = sd.pFreqData->Get(FD_RANGE_MAX);

    pMenu = new MenuController(hp, Background(sBound, Color(255,255,255)), pe.pExitProgram);

    pCntr = new ControlsController(hp, Background(sBound, Color(255,255,255)), NewSwitchEvent(pCurrControl, pMenu));

    pCred = new CreditsController(hp, Background(sBound, Color(255,255,255)), NewSwitchEvent(pCurrControl, pMenu));

    pCalChoice = new MenuController(hp, Background(sBound, Color(255,255,255)), NewSwitchEvent(pCurrControl, pMenu));

    pCal = new CalibrationController(hp, Background(sBound, Color(255,255,255)), NewSwitchEvent(pCurrControl, pMenu), tcd, sd);

    pCalChoice->AddEntry("CALIBRATE LOUDNESS", new LoundnessCalibrationEvent(
        LoudnessController(hp, Background(sBound, Color(255,255,255)), NewSwitchEvent(pCurrControl, pCalChoice), tcd, sd), pCurrControl));
    pCalChoice->AddEntry("CALIBRATE RANGE", NewSwitchEvent(pCurrControl, pCal));

    LevelStructure lvlStr;

    std::ifstream ifs(fp.GetParse("level.txt").c_str());
    if(ifs.fail())
        throw SimpleException("Cannot find level.txt");
    ifs >> lvlStr;
    if(ifs.fail())
        throw SimpleException("Cannot read level.txt");

    GameStarter st(hp_reg, Background(sBound, Color(255,255,255)), NewSwitchEvent(pCurrControl, pMenu), pCurrControl, tcd, sd);
    
    pLvlSl = new LevelSelect(hp, Background(sBound, Color(255,255,255)), NewSwitchEvent(pCurrControl, pMenu), 6, fp, st, "levels/", "list.txt");

    pMenu->AddEntry("START", new GameStartEvent(st, lvlStr));
    pMenu->AddEntry("LOAD", NewSwitchEvent(pCurrControl, pLvlSl));
    pMenu->AddEntry("CONTROLS", NewSwitchEvent(pCurrControl, pCntr));
    pMenu->AddEntry("CALIBRATE", NewSwitchEvent(pCurrControl, pCalChoice));
    pMenu->AddEntry("CREDITS", NewSwitchEvent(pCurrControl, pCred));
    pMenu->AddEntry("EXIT", pe.pExitProgram);

    pGenGuiLogo = new SimpleAnimator(hp, Background(sBound), NewSwitchEvent(pCurrControl, pMenu));

    pCurrControl = pGenGuiLogo;

    EventfulSequence es(pr("gengui"));
    es.SetEvent(11, new MakeSoundEvent(pSndMng, pr.GetSnd("click")));

    EventAnimation ea(es, true, 0, (100/nFrameRate), true);

    SP<Animate> pAn = new Animate(hp, AnimationPack(cc.fromF(fPoint(.5, .5))), ea, NewTerminatorEvent(pGenGuiLogo.GetRawPointer()));

    pGenGuiLogo->NewVictim(pAn);

    try
    {
        adc.openStream( NULL, &parameters, RT_READ_UNIT(),
                        sampleRate, &myBufferFrames, &record, &tcd, &options );
        adc.startStream();
    }
    catch(RtError& e)
    {
        throw SimpleException("SurfGameGlobalController", "<constructor>",
            "Audio error: " + e.getMessage());
    }
}

SurfGameGlobalController::~SurfGameGlobalController()
{
    try
    {
        adc.stopStream();
    }
    catch (RtError& e)
    {
        pMsg->Write(WT_ERROR, e.getMessage());
    }

    if (adc.isStreamOpen())
        adc.closeStream();
}

void SurfGameGlobalController::Update()
{
    SP<GameController> pCopyToEnsureNonDeleting = pCurrControl;

    pCurrControl->UpdateFull();
}

void SurfGameGlobalController::KeyDown(GuiKeyType nCode)
{
    pCurrControl->OnKey(nCode, false);
}

void SurfGameGlobalController::KeyUp(GuiKeyType nCode)
{
    pCurrControl->OnKey(nCode, true);
}
