#ifndef GLOBALS_H
#define GLOBALS_H

#ifdef WIN32
#pragma comment(lib, "SDL.lib")
#pragma comment(lib, "SDLmain.lib")
#pragma comment(lib, "SDL_image.lib")
//#pragma comment(lib, "SDL_ttf.lib")
#pragma comment(lib, "SDL_mixer.lib")
#endif

#include "SDL.h"
#include "SDL_Image.h"
//#include "SDL_ttf.h"
#include "SDL_mixer.h"
#include <string>
#include <fstream>
#include <iostream>
using namespace std;

#include "ENG_methods.h"


//screen constants
const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 768;
const int SCREEN_BPP = 32;

//game constants
const double GRAVITY = 250;

//GLOBALS CLASS
class EngineGlobals
{
public:
	//TEMPORARY DATA STORAGE
	void * micInput;

	EngineGlobals();
	~EngineGlobals();

	bool fullscreen();
	void cleanup();

	Logger errorLog;
	Timer time;

	//Surfaces image;
	//SurfaceDups imageDups;

	SDL_Surface* screen;
private:
	bool init();
	bool fullscreen(bool toggle);

	bool isFullScreen;

};


#endif