#include "ENG_camera.h"

Camera::Camera(SDL_Surface * screen)
{
	m_rect.w = screen->w;
	m_rect.h = screen->h;
	m_rect.x = 0;
	m_rect.y = 0;

	//temporary values
	m_maxDim.x = 0;
	m_maxDim.y = 0;
	m_maxDim.h = 2000;
	m_maxDim.w = 2000;

	m_screen = screen;
}

void Camera::update()
{
	//do camera actions here
};

void Camera::forceMove(int x, int y)
{
	m_rect.x = x;
	m_rect.y = y;
}

void Camera::center(DisplayObject & obj)
{

	//correct for level boundaries
	checkBoundary();
}

void Camera::forceCenter(DisplayObject * obj)
{	
	m_rect.x = ((PlayerSprite *)obj)->returnHit().x + ((PlayerSprite *)obj)->returnHit().w/2;
	m_rect.y = ((PlayerSprite *)obj)->returnHit().y + ((PlayerSprite *)obj)->returnHit().h/2;
}

//BORKEN
void Camera::follow(DisplayObject * obj)
{
	
	m_rect.x = ( 100*m_rect.x + ((HitObject *)obj)->returnHit().x )/101;
	m_rect.y = ( 100*m_rect.y + ((HitObject *)obj)->returnHit().y )/101;
	

	//m_rect.x = ((HitObject *)obj)->returnHit().x;
	//m_rect.y = ((HitObject *)obj)->returnHit().y;


	//checkBoundary();
}

//shakes the camera. 
//this method does not recenter camera so it will slowly drift over time
//camera must be recentered externally
void Camera::shake(int degree)
{
	m_rect.x += (rand()%100)*degree/100;
	m_rect.y += (rand()%100)*degree/100;
};


//BORKEN
void Camera::checkBoundary()
{
    //Keep the camera in bounds.
    if( m_rect.x < m_maxDim.x )
    {
       m_rect.x = m_maxDim.x;    
    }
    if( m_rect.y < m_maxDim.y )
    {
        m_rect.y = m_maxDim.y;    
    }

    if( m_rect.x > m_maxDim.x + m_maxDim.w - m_rect.w )
    {
        m_rect.x = m_maxDim.x + m_maxDim.w - m_rect.w;    
    }
    if( m_rect.y > m_maxDim.y + m_maxDim.h - m_rect.h )
    {
        m_rect.y = m_maxDim.y + m_maxDim.h - m_rect.h;    
    }    
}