#define _CRT_SECURE_NO_DEPRECATE

#include "ENG_display.h"
#include <list>
#include <map>
#include <set>
#include <stack>

extern EngineGlobals engine;

//BAD STUFF GET RID OF IT SOON!!!
//this function returns surface based on filename
//should be working
void getSurface(string filename, SDL_Surface * & main, SDL_Surface * & flip, bool freeSurface)
{
	struct SurfaceStruct
	{
		SDL_Surface * first;
		SDL_Surface * second;
	};

	static map<string,SurfaceStruct> surfaces;

	//add part
	if(!freeSurface)
	{		
		//if filename does not exist in map
		if(surfaces.find(filename) == surfaces.end())
		{
			SurfaceStruct temp;
			temp.first = loadImage(filename);
			temp.second = flipSurface(temp.first,2);
			surfaces[filename] = temp;
		}

		main = surfaces[filename].first;
		flip = surfaces[filename].second;
	}
	
	else
	{
		if(surfaces.find(filename) == surfaces.end())
		{
			for(map<string,SurfaceStruct>::iterator it = surfaces.begin(); it != surfaces.end(); it++)
			{
				SDL_FreeSurface(it->second.first);
				SDL_FreeSurface(it->second.second);
			}
			surfaces.clear();
		}
		else
		{
			
			SDL_FreeSurface(surfaces[filename].first);
			SDL_FreeSurface(surfaces[filename].second);
			surfaces.erase(filename);
		}
	}
	
}

//this function erases all surfaces, used for cleaning up I guess.
void freeAllSurfaces()
{
	SDL_Surface * temp;
	string tempString("SPRITE2.png");
	getSurface(tempString,temp,temp,true);
}




map<string,animState> * animStateMap()
{

	static map<string,animState> stateMap;
	stateMap["DEFAULT"] = DEFAULT;
	stateMap["JUMP"] = JUMP;
	stateMap["STAND"] = STAND;
	//FINISH MAP

	return &stateMap;
}

//recursively sets up frame graph
Frame * setUpFrames(XMLNode * data, int id, list<Frame*> * visited)
{
	//find id on list
	list<Frame*>::iterator it = visited->begin();

	while(it != visited->end() && (*it)->id != id)
	{
		it++;
	}
	
	//if id is NOT found
	if(it == visited->end())
	{
		//create temp frame
		Frame * setFrame = new Frame;
		//get xml data of <frame id="id">
		char buffer[5];
		XMLNode firstFrame = data->getChildNodeWithAttribute("frame","id",_itoa(id,buffer,10));
		//get all info from the tag


		setFrame->rectDraw.x = atoi(firstFrame.getAttribute("x"));
		setFrame->rectDraw.y = atoi(firstFrame.getAttribute("y"));
		setFrame->rectDraw.w = atoi(firstFrame.getAttribute("w"));
		setFrame->rectDraw.h = atoi(firstFrame.getAttribute("h"));
		setFrame->id = atoi(firstFrame.getAttribute("id"));
		setFrame->time = atoi(firstFrame.getAttribute("time"));

		//use this to test if the attribute exists.
		//implement later for default values 
		//if(firstFrame.getAttribute("velx") == NULL);

		if(firstFrame.getAttribute("velx") == NULL)
			setFrame->velx = 0;
		else
			setFrame->velx = atoi(firstFrame.getAttribute("velx"));
		if(firstFrame.getAttribute("vely") == NULL)
			setFrame->vely = 0;
		else
			setFrame->vely = atoi(firstFrame.getAttribute("vely"));

		//set state
		if(firstFrame.getAttribute("state") == NULL)
			setFrame->state = DEFAULT;
		else
			setFrame->state = (*animStateMap())[firstFrame.getAttribute("state")];



		/*
		setFrame->rectHit = setFrame->rectDraw;
		setFrame->rectHit.x = 0;
		setFrame->rectHit.y = 0;
		*/

		setFrame->rectHit.x = 30;
		setFrame->rectHit.y = 40;
		setFrame->rectHit.w = 80;
		setFrame->rectHit.h = 103;
		

		//loop through each <next>tag
		int i=0;

		//put frame onto list of frames we've finished
		visited->push_back(setFrame);

		while(!firstFrame.getChildNode(i).isEmpty())
		{
			//cout << "ONCE";
			setFrame->map[ (*animStateMap())[ firstFrame.getChildNode(i).getAttribute("state") ] ] = setUpFrames(data, atoi(firstFrame.getChildNode(i).getAttribute("id")), visited);
			i++;
		}

		//return the frame we are werking on
		return setFrame;
	}
	//else id IS found we return a pointer to that frame
	else
	{
		return (*it);
	}
};

//setup graph
PlayerSprite::PlayerSprite(XMLNode data)
{
	loadFrames(data);
	lastUpdate = 0;
	lastFrameUpdate = 0;

	m_locVel.x = 0;
	m_locVel.y = 0;
	m_locVel.xVel = 0;
	m_locVel.yVel = 0;

	m_locVel.dir = false;

	inControl = false;

	m_id.type = "PlayerSprite";
};

bool PlayerSprite::loadFrames(XMLNode data)
{
	XMLNode objectNode = data.getChildNodeWithAttribute("sprite","name","player");
	getSurface(objectNode.getAttribute("file"),m_mainSurface,m_flipSurface);

	list<Frame *> * tempFrameList = new list<Frame *>;
	m_currentFrame = setUpFrames(&objectNode,1,tempFrameList);
	delete tempFrameList;

	return true;
};

PlayerSprite::~PlayerSprite()
{
	deleteNodes(m_currentFrame);
};

void PlayerSprite::deleteNodes(Frame * node)
{

	if(m_currentFrame != NULL)
	{
		set<Frame *> visited;
		stack<Frame *> stack;

		//push starting node onto stack and visited list
		stack.push(node);
		visited.insert(node);

		while(!stack.empty())
		{

			//pop off top of stack so we can add children and delete it.
			Frame * temp = stack.top();
			stack.pop();
			
			//go through each of children
			for(map<animState, Frame*>::iterator it = temp->map.begin(); it != temp->map.end(); it++)
			{
				//if child is not NULL
				if(it->second != NULL)
				{
					//if we haven't visited the node before
					if(visited.find(it->second) == visited.end())
					{
						//push it into stack
						stack.push(it->second);
						//add it to visited list
						visited.insert(it->second);
					}
				}
			}
			//now delete the node we just finished processing processing
			delete temp;
		}
	}
};

void PlayerSprite::handleInput(SDL_Event &keyEvent, Uint8 *keystates)
{
	if(inControl)
	{
		if(keystates[SDLK_LEFT])
		{
			m_animState = DEFAULT;
			m_locVel.dir = true;
		}
		else if(keystates[SDLK_RIGHT])
		{
			m_animState = DEFAULT;
			m_locVel.dir = false;
		}
		else
		{
			//if not jumping
			if(m_actualState != JUMP && m_animState != JUMP)
			{
				m_animState = STAND;
			}
		}

		if(keystates[SDLK_UP])
		{

			m_animState = JUMP;
			//m_locVel.yVel = -250;
			//set state to jump
		}
		else
		{
			//cout << "default" << endl;
			//m_animState = DEFAULT;
			//m_actualState = DEFAULT;
		}
			
	}
	else
	{
		m_locVel.xVel = m_locVel.yVel = 0;
		m_animState = STAND;
	}

};

void PlayerSprite::update()
{
	int ticks  = engine.time.get_ticks();
	if(lastFrameUpdate == 0)
		lastFrameUpdate = ticks;

	//gravity
	if(m_locVel.yVel < 250)
	{
		m_locVel.yVel += (double)GRAVITY*(double)(ticks - lastUpdate)/(double)1000;
		//if(m_locVel.yVel > 250)
		//	m_locVel.yVel = 250;
	}

	//cout <<	m_currentFrame->velx << " " << m_currentFrame->vely << endl;

	//set velocity
	if(m_currentFrame->velx != -1)
	{
		if(m_locVel.dir)
			m_locVel.xVel = -m_currentFrame->velx;
		else
			m_locVel.xVel = m_currentFrame->velx;

	}

	//set velocity
	/*
	if(m_currentFrame->velx != -1)
	{
		if(m_locVel.dir)
		{
			m_locVel.xVel -= m_currentFrame->velx;
		}
		else
		{
			m_locVel.xVel += m_currentFrame->velx;
		}

	}
	*/

	if(m_currentFrame->vely != -1)
		m_locVel.yVel = -m_currentFrame->vely;

	//position
	m_locVel.y += m_locVel.yVel * (double)(ticks - lastUpdate)/(double)1000;
	m_locVel.x += m_locVel.xVel * (double)(ticks - lastUpdate)/(double)1000;

	
	//hit detection for ground
	//GARBAGE
	/*
	if( m_locVel.y > -140)
	{
		m_locVel.y = -140;
		//if jumping, set to stand
		if(m_actualState == JUMP && m_animState != STAND)
			m_animState = STAND;
	}*/


	//gives FPS
	//engine.error.log(1000/m_time.get_last());

	//set frames to render
	while(ticks-lastFrameUpdate >= m_currentFrame->time)
	{
		//advance time
		lastFrameUpdate += m_currentFrame->time;
		//if we take special path
		if(locate())
		{				
			m_actualState = m_currentFrame->state;
			m_currentFrame = m_currentFrame->map[m_animState];

		}
		//else go default path
		else
		{
			m_currentFrame = m_currentFrame->map[DEFAULT];
		}
	}

	//update the timer
	lastUpdate = ticks;
};

//teleports player to location x y
void PlayerSprite::teleport(int x, int y)
{
	m_locVel.x = x;
	m_locVel.y = y;
};


SDL_Rect PlayerSprite::returnHit()
{
	//get hit box coords relative to draw coords.
	m_rect = m_currentFrame->rectHit;

	//make hit box coords relative to actual coordinates (i.e. add on draw coordinates)
	m_rect.x += (int)m_locVel.x;
	m_rect.y += (int)m_locVel.y;

	return m_rect;
};


void PlayerSprite::draw(Camera * drawCam)
{
	//cout << returnHit().x - drawCam->m_rect.x + drawCam->m_rect.w<< " " << returnHit().y - drawCam->m_rect.y + drawCam->m_rect.h<< " " << endl;
	//cout << returnHit().x << " " << m_locVel.xVel << endl;

	applySurface(
		returnAbsDraw().x - drawCam->m_rect.x + drawCam->m_rect.w/2, 
		returnAbsDraw().y - drawCam->m_rect.y + drawCam->m_rect.h/2,
		returnSurface(),
		drawCam->m_screen,
		&returnDraw());
};

bool PlayerSprite::locate()
{
	map<animState, Frame*>::iterator it;
	it = m_currentFrame->map.find(m_animState);
	if(it == m_currentFrame->map.end())
		return false;
	else
		return true;
}

SDL_Rect PlayerSprite::returnDraw() 
{ 
	if(m_locVel.dir)
		return getRect( m_currentFrame->rectDraw.w, m_currentFrame->rectDraw.h, m_mainSurface->w - m_currentFrame->rectDraw.x - m_currentFrame->rectDraw.w, m_currentFrame->rectDraw.y);
	else
		return m_currentFrame->rectDraw;
};


SDL_Rect standardHit(const SDL_Rect & ourRect, const SDL_Rect & hitRect)
{
	SDL_Rect temp = ourRect;

	//UP DOWN LEFT RIGHT	
	//has clipping issues

	//cout << ourRect.y << " " << ourRect.h << " " << hitRect.y << endl;

	int right = ourRect.x + ourRect.w - hitRect.x;
	int left = ourRect.y + ourRect.h - hitRect.y;

	/*
	if(left > right)
	{
		temp.x -= ourRect.x + ourRect.w - hitRect.x;
	}
	else
	{ 
		temp.y -= ourRect.y + ourRect.h - hitRect.y;
	}*/

		temp.y -= ourRect.y + ourRect.h - hitRect.y;


	/*

	//RIGHT
	if(ourRect.x + ourRect.w > hitRect.x)
	{
		temp.x -= ourRect.x + ourRect.w - hitRect.x;
	}

	//UP
	if(ourRect.y + ourRect.h > hitRect.y)
	{
		temp.y -= ourRect.y + ourRect.h - hitRect.y;
	}
	*/

	//returns new hit box coordinates
	return temp;
};

void PlayerSprite::recordHit(HitObject * obj)
{
	if(obj->getId().type == "WallObject")
	{
		 SDL_Rect temp = standardHit(this->returnHit(), obj->returnHit());
		 //m_locVel.x = temp.x - returnHit().x;

		 m_locVel.y = temp.y - m_currentFrame->rectHit.y;
		 m_locVel.x = temp.x - m_currentFrame->rectHit.x;


		 if(m_actualState == JUMP && m_animState != STAND)
				m_animState = STAND;
	}

	if(obj->getId().type == "SoundWave")
	{
		//need to disect the wave

	}
};

void FollowMe::handleInput(SDL_Event & keyEvent, Uint8 * keystates)
{
	int ticks = engine.time.get_ticks();

	if(keystates[SDLK_LEFT])
		m_rect.x -= (ticks-lastUpdate)*FOLLOW_SPEED;


	if(keystates[SDLK_RIGHT])
		m_rect.x += (ticks-lastUpdate)*FOLLOW_SPEED;

	if(keystates[SDLK_UP])
		m_rect.y -= (ticks-lastUpdate)*FOLLOW_SPEED;

	if(keystates[SDLK_DOWN])
		m_rect.y += (ticks-lastUpdate)*FOLLOW_SPEED;

	lastUpdate = ticks;
};

void FollowMe::update()
{
};