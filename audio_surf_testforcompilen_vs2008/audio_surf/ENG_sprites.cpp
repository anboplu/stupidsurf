#include "ENG_display.h"
#include <list>
#include <map>
#include <set>
#include <stack>

extern EngineGlobals engine;

//setup graph
GenericSprite::GenericSprite(XMLNode data, int uniqueId)
{
	//set up id info
	m_id.construct(data, 1);

	loadFrames(data);

	//silly variable initialization stuff
	lastUpdate = 0;
	lastFrameUpdate = 0;

	m_locVel.dir = false;
	m_locVel.x = 0;
	m_locVel.y = 0;
	m_locVel.xVel = 0;
	m_locVel.yVel = 0;

	inControl = false;
};

bool GenericSprite::loadFrames(XMLNode data)
{
	//get sprite with attribute name = player
	getSurface(data.getAttribute("file"),m_mainSurface,m_flipSurface);


	//new frame chart thingy 
	m_graph = new AnimGraph( data.getAttribute("name") );
	//grab frame, update timer whatever...
	m_graph->grabFrame(engine.time.get_ticks(),m_animState, m_currentFrame);

	return true;
};

GenericSprite::~GenericSprite()
{
	
};


void GenericSprite::handleInput(SDL_Event &keyEvent, Uint8 *keystates)
{
};

void GenericSprite::update()
{
	if(m_graph->grabFrame(engine.time.get_ticks(),m_animState, m_currentFrame))
	{
		m_actualState = m_animState;
	};



};

//teleports player to location x y
void GenericSprite::teleport(int x, int y)
{
	m_locVel.x = x;
	m_locVel.y = y;
};


SDL_Rect GenericSprite::returnHit()
{
	m_rect = m_currentFrame->rectHit;

	m_rect.x = (int)m_locVel.x;
	m_rect.y = (int)m_locVel.y;

	return m_rect;
};


void GenericSprite::draw(Camera * drawCam)
{

	applySurface(
		returnHit().x - drawCam->m_rect.x + drawCam->m_rect.w/2, 
		returnHit().y - drawCam->m_rect.y + drawCam->m_rect.h/2,
		returnSurface(),
		drawCam->m_screen,
		&returnDraw());
		
};


SDL_Rect GenericSprite::returnDraw() 
{ 
	if(m_locVel.dir)
		return getRect( m_currentFrame->rectDraw.w, m_currentFrame->rectDraw.h, m_mainSurface->w - m_currentFrame->rectDraw.x - m_currentFrame->rectDraw.w, m_currentFrame->rectDraw.y);
	else
		return m_currentFrame->rectDraw;
};

void GenericSprite::recordHit(HitObject * obj)
{
	// SDL_Rect temp = standardHit(this->returnHit(), obj->returnHit());
	// m_locVel.x = temp.x;
	// m_locVel.y = temp.y;
};



Surfer::Surfer(XMLNode data, int uniqueId):GenericSprite(data,uniqueId)
{
	hitLast = engine.time.get_ticks();
	lastPos = returnHit();
};

void Surfer::update()
{

	int ticks  = engine.time.get_ticks();

	//advance animation
	if(m_graph->grabFrame(engine.time.get_ticks(),m_animState, m_currentFrame))
	{
		m_actualState = m_animState;
	};

	//set gravity
	m_locVel.yVel += GRAVITY;
	if(m_locVel.yVel > 300)
		m_locVel.yVel = 300;

	//set new position based on velocity
	m_locVel.y += m_locVel.yVel*returnDiff(ticks,lastUpdate)/1000.0;

	//set last position to current possition
	lastPos = returnHit();

	//update last update
	lastUpdate = ticks;


};

void Surfer::recordHit(HitObject *obj)
{
	if(obj->getId().type == "WallObject")
	{
		 SDL_Rect temp = standardHit(this->returnHit(), obj->returnHit());
		 //m_locVel.x = temp.x - returnHit().x;



		 //BUGS UP THE WAZOO
		 //m_locVel.y = temp.y - m_currentFrame->rectHit.y;
		 //m_locVel.x = temp.x - m_currentFrame->rectHit.x;


		 m_locVel.yVel -= ((obj->returnHit().y) - (this->returnHit().y+this->returnHit().h)) * returnDiff(hitLast,engine.time.get_ticks())/1000;

		 cout << ((obj->returnHit().y) - (this->returnHit().y+this->returnHit().h)) << " " << returnDiff(hitLast,engine.time.get_ticks())/1000 << " " << m_locVel.yVel << endl;

	}

	if(obj->getId().type == "SoundWave")
	{
		//need to disect the wave

	}

	hitLast = engine.time.get_ticks();
};