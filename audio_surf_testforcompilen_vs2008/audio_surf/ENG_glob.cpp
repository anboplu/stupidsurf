#include "ENG_glob.h"

EngineGlobals engine;

EngineGlobals::EngineGlobals():errorLog("log.txt")
{
	//SET TMP DATAS TORAGE NULL
	micInput = NULL;

	//not full screen
	isFullScreen = false;

	//init SDL
	init();

	//reset timer
	time.reset();
};

EngineGlobals::~EngineGlobals()
{
	Mix_CloseAudio(); 
//	TTF_Quit();
	SDL_Quit();	
}

void EngineGlobals::cleanup()
{
};

bool EngineGlobals::fullscreen()
{
	if(isFullScreen)
		return fullscreen(false);
	else
		return fullscreen(true);
};

bool EngineGlobals::fullscreen(bool toggle)
{
	if(toggle)
		screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE | SDL_RESIZABLE | SDL_FULLSCREEN ); 
	else
		screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE | SDL_RESIZABLE ); 

	if(screen == NULL)
		return false;

	isFullScreen = !isFullScreen;

	return true;
};

bool EngineGlobals::init()
{
	///Initialize the SDL library
	if( SDL_Init(SDL_INIT_EVERYTHING) < 0 ) 
	{
		//print error
		errorLog.log("SDL init ERROR");
		//return
		return false;
	}

	//initialize screen
	screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE | SDL_RESIZABLE ); 
	if ( screen == NULL ) 
	{
		//print error
		errorLog.log("video init ERROR");
		//return
		return false;
	}

    /*/initialize TTF
	if( TTF_Init() == -1 )
	{
		//print error
		errorLog.log("TTF init ERROR");
		//return
		return false;
	}*/

	//initialize mixer
	if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 ) 
	{
		//print error
		errorLog.log("mixer init ERROR");
		//return
		return false;
	}

	//get video info, good for debugging I guess
	//const SDL_VideoInfo* videoinfo;
	//videoinfo = SDL_GetVideoInfo();

	return true;
}
