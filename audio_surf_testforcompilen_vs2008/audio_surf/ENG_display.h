//future additions
//scriptable sprite?

#ifndef DISPLAY_H
#define DISPLAY_H

#include "ENG_glob.h"
#include "ENG_camera.h"
#include "ENG_methods.h"
#include "ENG_graph.h"
#include "xmlParser.h"
#include <map>
#include <string>
#include <list>

class Camera;
class AnimGraph;

//identifier struct
struct IdStruct
{
	//it is level editor's job to make sure no objects have the same id

	//specifically the name of the object as it appears in the xml file
	string name;

	//what type of object is it  (i.e. class name)
	string type;

	//unique id under type or maybe name. I dunno.	
	int id;

	void construct(XMLNode & data, int uniqueId)
	{
		name = data.getAttribute("name");
		type = data.getAttribute("type");
		id = uniqueId;
	};
};

//specific to playerSprite class for now
enum animState
{
	//the standard
	DEFAULT,
	STAND,
	RUN,
	JUMP,
	CROUCH,
	STANDUP,

	//surfer
	UP,
	DOWN
};

//specific to playerSprite
//frame graph node
struct Frame
{
	unsigned int id;

	//rect
	SDL_Rect rectDraw;
	SDL_Rect rectHit;

	//time to spend on this animation
	int time; 

	//speed assosciated with each frame
	int vely, velx;

	//state associated with frame
	animState state;

	//map of string to Frame pointer...
	map<animState, Frame*> map;
};

//some functions

//temporary surface management function
//turn this into a class
void getSurface(string filename, SDL_Surface * & main, SDL_Surface * & flip, bool freeSurface = false);
//gives a map from strings to animState enum
map<string,animState> * animStateMap();
//sets up frames... probably make a class out of this
Frame * setUpFrames(XMLNode * data, int id, list<Frame*> * visited);
//standard hit detection. return corrected hit box
SDL_Rect standardHit(const SDL_Rect & ourRect, const SDL_Rect & hitRect);


//location and velocity
struct LocVel { double x, y, xVel, yVel; bool dir; };

//is just an object. may or may not make use of any or all routines
class DisplayObject
{
public:
	//probably do nothing
	DisplayObject() {};
	~DisplayObject() {};

	//identifiers
	IdStruct getId() { return m_id; };

	//common methods
	virtual void update() {};
	virtual void handleInput(SDL_Event & keyEvent, Uint8 * keystates = NULL){};
	virtual void draw(Camera * drawCam) {};

protected:
	IdStruct m_id;

};

//these objects have hit detection added
//also adds location
class HitObject : public DisplayObject 
{
public:
	HitObject() { m_rect.x = m_rect.y = m_rect.h = m_rect.w = 0; }; 
	virtual void recordHit(HitObject * obj){};
	virtual SDL_Rect returnHit() { return m_rect; };
	virtual void teleport(int x, int y) { m_rect.x = x; m_rect.y = y; };
protected:
	//this is object location 
	SDL_Rect m_rect;
};

//class sprite
//adds animation graph

//very specific derived class. Generalize later 
class GenericSprite : public HitObject 
{
public:
	GenericSprite(XMLNode data, int uniqueId);
	~GenericSprite();

	//base routines
	virtual void handleInput(SDL_Event & keyEvent, Uint8 * keystates = NULL);
	virtual void draw(Camera * drawCam);
	virtual void update();

	//Hit detection stuff
	virtual SDL_Rect returnHit();
	virtual void recordHit(HitObject * obj);

	//teleports player to x, y
	virtual void teleport(int x, int y);
	virtual void turn(bool left) { m_locVel.dir = left; };

	//temp for screwing around
	bool inControl;

protected:
	//setup
	bool loadFrames(XMLNode data);
	//animGraph
	AnimGraph * m_graph;

	//location velocity direction
	LocVel m_locVel;

	//time state
	animState m_animState;
	animState m_actualState;
	int lastUpdate;
	int lastFrameUpdate;

	//graph
	Frame * m_currentFrame;

	//source surfaces
	SDL_Surface * m_mainSurface;
	SDL_Surface * m_flipSurface;

	//draw routine helpers
	SDL_Surface * returnSurface() { return m_locVel.dir ? m_flipSurface :  m_mainSurface; };
	SDL_Rect returnDraw();

	
};


//very specific derived class. Generalize later 
class PlayerSprite : public HitObject 
{
public:
	PlayerSprite(XMLNode data);
	~PlayerSprite();

	//base routines
	virtual void handleInput(SDL_Event & keyEvent, Uint8 * keystates = NULL);
	virtual void draw(Camera * drawCam);
	virtual void update();

	//Hit detection stuff
	virtual SDL_Rect returnHit();
	virtual void recordHit(HitObject * obj);

	//teleports player to x, y
	virtual void teleport(int x, int y);

	//temp for screwing around
	bool inControl;

private:
	//location velocity direction
	LocVel m_locVel;

	//time state
	animState m_animState;
	animState m_actualState;
	int lastUpdate;
	int lastFrameUpdate;

	//graph
	Frame * m_currentFrame;
	bool locate();

	//source surfaces
	SDL_Surface * m_mainSurface;
	SDL_Surface * m_flipSurface;

	//draw routine helpers
	SDL_Surface * returnSurface() { return m_locVel.dir ? m_flipSurface :  m_mainSurface; };
	//returns draw coordinates of source surface
	SDL_Rect returnDraw();

	//BROKE?
	//returns absolute draw coords, x and y only
	SDL_Rect returnAbsDraw() { return getRect( 0, 0, returnHit().x - m_currentFrame->rectHit.x, returnHit().y - m_currentFrame->rectHit.y); };

	//loading and deleting 
	bool loadFrames(XMLNode data);
	void deleteNodes(Frame * node);
};


class WallObject : public GenericSprite
{
public:
	//randomizes frames
	//void unsych() { m_graph->speedup(rand()%200); };
private:
};

class Surfer : public GenericSprite
{
public:
	Surfer(XMLNode data, int uniqueId);
	void update();
	void recordHit(HitObject *obj);
private:
	int hitLast;
	SDL_Rect lastPos;
};

class FollowMe : public HitObject
{
public:
	FollowMe() { FOLLOW_SPEED = 1; };
	void handleInput(SDL_Event & keyEvent, Uint8 * keystates = NULL);
	void update();
private:
	int lastUpdate;
	float FOLLOW_SPEED;
};



#endif