/*

#include "RtAudio.h"
#include <iostream>

#include <cmath>
#include <cstdlib>

using std::rand;
// math stuff
using std::cos;
using std::abs;
using std::exp;
using std::log10;
// iostream stuff
using std::cout;
using std::endl;

#define PI 3.14159265358979323844
// change the defines if you want to
#define SAMPLERATE 44100
#define BUFFERSIZE 8820
#define FREQUENCY 3000
#define NOISE 0.05
#define SIGNALVOLUME 0.8

float goertzel(double *x, int N, float frequency, int samplerate) {
    float Skn, Skn1, Skn2;
    Skn = Skn1 = Skn2 = 0;
    
    for (int i=0; i<N; i++) {
    Skn2 = Skn1;
    Skn1 = Skn;
    Skn = 2*cos(2*PI*frequency/samplerate)*Skn1 - Skn2 + x[i];
    }
    
    float WNk = exp(-2*PI*frequency/samplerate); // this one ignores complex stuff
    //float WNk = exp(-2*j*PI*k/N);
    return (Skn - WNk*Skn1);
}

float power(float value) {
    return 20*log10(abs(value));
}


int record2( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
         double streamTime, RtAudioStreamStatus status, void *userData )
{
  if ( status )
    std::cout << "Stream overflow detected!" << std::endl;

  // Do something with the data in the "inputBuffer" buffer.

  //std::cout << ((double *)inputBuffer)[5] << "\n";


	//cout << "Trying freq: " << 2000/2 << "Hz  ->  dB: " << (goertzel( (double *)inputBuffer, nBufferFrames, 2000/2, SAMPLERATE)) << endl;



	for(int freq = 1050; freq < 2100; freq+=100)
	{
		if((goertzel( (double *)inputBuffer, nBufferFrames, freq/2, SAMPLERATE)) > 100)
			cout << freq << endl;
	}
  



	int stepsize = FREQUENCY/10;


  return 0;
}

int main()
{
  RtAudio adc;
  if ( adc.getDeviceCount() < 1 ) {
    std::cout << "\nNo audio devices found!\n";
    exit( 0 );
  }

  RtAudio::StreamParameters parameters;
  parameters.deviceId = adc.getDefaultInputDevice();
  parameters.nChannels = 2;
  parameters.firstChannel = 0;
  unsigned int sampleRate = 44100;
  unsigned int bufferFrames = 1024*2; // 256 sample frames

  try {
    adc.openStream( NULL, &parameters, RTAUDIO_FLOAT64,
                    sampleRate, &bufferFrames, &record2 );
    adc.startStream();
  }
  catch ( RtError& e ) {
    e.printMessage();
    exit( 0 );
  }
  
  char input;
  std::cout << "\nRecording ... press <enter> to quit.\n";
  std::cin.get( input );

  try {
    // Stop the stream
    adc.stopStream();
  }
  catch (RtError& e) {
    e.printMessage();
  }

  if ( adc.isStreamOpen() ) adc.closeStream();

  return 0;
}

*/