#ifndef GRAPH_H
#define GRAPH_H

#include "ENG_glob.h"
#include "ENG_display.h"
#include "xmlParser.h"
#include <list>
#include <string>
#include <set>
#include <stack>

struct Frame;
enum animState;

class AnimGraph
{
public:
	AnimGraph(string objectName);
	~AnimGraph();

	//returns true if state changes
	bool grabFrame(const int newTime, const animState state, Frame * & changeFrame);

	//speeds up
	void speedup(int amount) { lastFrameUpdate += amount; };
	
private:
	bool loadFrames(XMLNode objectNode);
	bool locate(animState state);
	void deleteNodes(Frame * node);



	Frame * m_currentFrame;
	int lastFrameUpdate;
};

#endif