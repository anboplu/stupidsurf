# convert a .png image file <strong class="highlight">to</strong> a .bmp image file using PIL

import os
from PIL import Image

def isPng(img):
    if img[len(img)-3:len(img)] == "png":
        return True;
    else:
        return False;
def convertImage(path,fn):
    c = Image.open(path+"\\"+fn)
    c.save(fn+".bmp")
    #print path+"\\"+fn[0:len(fn)-3]+"bmp"
    print c
    try:
        if len(c.split()) == 4:
            r,g,b,a = c.split()
            c = Image.merge("RGB",(r,g,b))
    except: print "failed merge", fn
    c.save(path+"\\"+fn[0:len(fn)-3]+"bmp")

def convertRecursive(dir):
    basedir = dir
    #print "Files in ", os.path.abspath(dir), ": "
    subdirlist = []
    for item in os.listdir(dir):
        if isPng(item):
            #convert that stuff
            try:
                convertImage( dir,item)
            except: print "failed conversion", item
        else:
            subdirlist.append(os.path.join(basedir, item))
    for subdir in subdirlist:
        try:
            convertRecursive(subdir)
        except: print "failed", subdir
convertRecursive("")
